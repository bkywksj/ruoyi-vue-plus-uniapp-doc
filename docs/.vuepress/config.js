module.exports = {
    title: 'plus-uniapp', // 设置网站标题
    description: '多功能校园平台',
    base: '/',
    dest: 'dist',
    markdown: {
        //lineNumbers: true
    },
    plugins: {
        'demo-container': {
            component: 'CustomDemoBlock'
        }
    },
    locales: {
        // 键名是该语言所属的子路径
        // 作为特例，默认语言可以使用 '/' 作为其路径。
        '/': {
            lang: 'zh-CN' // 将会被设置为 <html> 的 lang 属性
        }
    },
    evergreen: true, // 只适配现代浏览器
    // <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    configureWebpack: (config, isServer) => {
        const defaultConfig = {
            node: {
                global: true
            }
        }

        // 只有在发布(isServer=true)的时候才进行此修改操作，否则在本地预览时出问题
        if (isServer) {
            const newConfig = {
                output: {
                    filename: `assets/js/[name].${+new Date()}.[chunkhash].js`
                }
            }
            // 修改客户端的 webpack 配置
            // 加入一个时间戳，让每次编译时，文件都不一样，也即每次发版本，都强行更新所有文件
            return Object.assign(defaultConfig, newConfig)
        } else {
            return defaultConfig;
        }
    },
    head: [
        ['meta', {
            name: 'viewport',
            content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
        }],
        ['meta', {
            name: 'keywords',
            content: 'ruoyi-vue-plus-uniapp开发文档'
        }]
    ],

    themeConfig: {
        baseUrl: 'https://doc.ruoyikj.top',
        search: true, // 是否显示顶部搜索框
        searchPlaceholder: '搜索文档关键字',
        sidebarDepth: 0,
        nav: [
            {
                text: '演示地址',
                link: '/demo/demo'
            },
            {
                text: '开发文档',
                link: '/develop/intro'
            },
            {
                text: '关于我们',
                link: '/about/about'
            },
        ],
        sidebar: {
            '/develop/': [{
                title: '起步',
                collapsable: false,
                sidebarDepth: 0,
                children: [
                    ['/develop/start/xmjg', '项目结构'],
                    ['/develop/run/xmyx', '项目运行']
                ]
            },
                {
                    title: '业务设计',
                    collapsable: false,
                    sidebarDepth: 0,
                    children: [
                        ['/develop/design/db', '数据库设计'],
                        ['/develop/design/genConfig', '配置代码生成'],
                        ['/develop/design/generate', '代码生成']
                    ]
                },
                {
                    title: '后端开发',
                    collapsable: false,
                    sidebarDepth: 0,
                    children: [
                        ['/develop/back/login', '登录开发'],
                        ['/develop/back/sjqx', '数据权限'],
                    ]
                },
                {
                    title: '前端开发',
                    collapsable: false,
                    sidebarDepth: 0,
                    children: [
                        ['/develop/front/cdate', '日期选择'],
                        ['/develop/front/checkbox', '复选框'],
                        ['/develop/front/choose', '下拉选择'],
                        ['/develop/front/fupload', '文件上传'],
                        ['/develop/front/imgupload', '图片上传'],
                        ['/develop/front/in', '输入框'],
                        ['/develop/front/radio', '单选框'],
                        ['/develop/front/rich', '富文本'],
                    ]
                },
                {
                    title: 'uniapp开发',
                    collapsable: false,
                    sidebarDepth: 0,
                    children: [
                        ['/develop/uniapp/gs', '概述'],
                        ['/develop/uniapp/myView', 'myView容器'],
                        ['/develop/uniapp/myScrollView', 'myScrollView可滑动view'],
                        ['/develop/uniapp/myCard', 'myCard卡片'],
                        ['/develop/uniapp/myContainer', 'myContainer容器'],
                        ['/develop/uniapp/myImg', 'myImg图片'],
                        ['/develop/uniapp/myImgList', 'myImgList图片列表'],
                        ['/develop/uniapp/myImgSwiper', 'myImgSwiper图片轮播'],
                        ['/develop/uniapp/mySwiper', 'mySwiper轮播'],
                        ['/develop/uniapp/myFileList', 'myFileList文件列表'],
                        ['/develop/uniapp/myText', 'myText文本'],
                        ['/develop/uniapp/myTextArea', 'myTextArea长文本'],
                        ['/develop/uniapp/myTitle', 'myTitle标题'],
                        ['/develop/uniapp/myContent', 'myContent内容'],
                        ['/develop/uniapp/myBtn', 'myBtn按钮'],
                        ['/develop/uniapp/myButton', 'myButton按钮'],
                        ['/develop/uniapp/myBottomBtn', 'myBottomBtn底部按钮'],
                        ['/develop/uniapp/myViewButton', 'myViewButton视图按钮'],
                        ['/develop/uniapp/myLoading', 'myLoading加载动画'],
                        ['/develop/uniapp/myPage', 'myPage分页'],
                        ['/develop/uniapp/mySearch', 'mySearch搜索'],
                        ['/develop/uniapp/mySearchHistory', 'mySearchHistory搜索历史'],
                        ['/develop/uniapp/myNodata', 'myNodata无数据'],
                        ['/develop/uniapp/myForm', 'myForm表单容器'],
                        ['/develop/uniapp/formAdd', 'formAdd表单添加'],
                        ['/develop/uniapp/formAvatar', 'formAvatar头像'],
                        ['/develop/uniapp/formCertUpload', 'formCertUpload证件上传'],
                        ['/develop/uniapp/formCustom', 'formCustom自定义表单'],
                        ['/develop/uniapp/formFileUpload', 'formFileUpload文件上传'],
                        ['/develop/uniapp/formImgUpload', 'formImgUpload图片上传'],
                        ['/develop/uniapp/formInput', 'formInput输入框'],
                        ['/develop/uniapp/formNumberBox', 'formNumberBox数字输入框'],
                        ['/develop/uniapp/formSelect', 'formSelect下拉选择'],
                        ['/develop/uniapp/formSwitch', 'formSwitch开关'],
                        ['/develop/uniapp/formMp', 'formMp富文本'],
                        ['/develop/uniapp/mp-html', 'mp-html富文本展示'],
                        ['/develop/uniapp/myRightAdd', 'myRightAdd右侧添加'],
                        ['/develop/uniapp/myNav', 'myNav导航'],
                        ['/develop/uniapp/myNavBg', 'myNavBg导航背景'],
                        ['/develop/uniapp/myTabs', 'myTabs标签'],
                        ['/develop/uniapp/myChildTabs', 'myChildTabs子标签'],
                        ['/develop/uniapp/myTags', 'myTags标签'],
                        ['/develop/uniapp/msgTips', 'msgTips消息提示'],
                        ['/develop/uniapp/myAddTips', 'myAddTips收藏提示'],
                        ['/develop/uniapp/myAudio', 'myAudio音频'],
                        ['/develop/uniapp/myAuth', 'myAuth授权'],
                        ['/develop/uniapp/myPop', 'myPop弹窗'],
                        ['/develop/uniapp/myModal', 'myModal模态框'],
                        ['/develop/uniapp/myBottomCheck', 'myBottomCheck底部审核'],
                        ['/develop/uniapp/myBottomPop', 'myBottomPop底部弹窗'],
                        ['/develop/uniapp/myDiamon', 'myDiamon金刚区'],
                        ['/develop/uniapp/myShops', 'myShops商铺'],
                        ['/develop/uniapp/myMenu', 'myMenu菜单'],
                        ['/develop/uniapp/myMenuHt', 'myMenuHt菜单管理'],
                        ['/develop/uniapp/myMenuItem', 'myMenuItem菜单项'],
                        ['/develop/uniapp/myCart', 'myCart购物车'],
                        ['/develop/uniapp/myCartGoods', 'myCartGoods购物车商品'],
                        ['/develop/uniapp/myCartStatus', 'myCartStatus购物车状态'],
                        ['/develop/uniapp/myGoods', 'myGoods商品'],
                        ['/develop/uniapp/myOrderGoods', 'myOrderGoods订单商品'],
                        ['/develop/uniapp/myOrder', 'myOrder订单'],
                        ['/develop/uniapp/myShopOrder', 'myShopOrder商城订单'],
                        ['/develop/uniapp/myCheck', 'myCheck审核'],
                        ['/develop/uniapp/myInfo', 'myInfo信息'],
                        ['/develop/uniapp/myMask', 'myMask遮罩'],
                        ['/develop/uniapp/mySelect', 'mySelect选择'],
                        ['/develop/uniapp/myStatNum', 'myStatNum统计数字'],
                        ['/develop/uniapp/myLinkman', 'myLinkman联系人'],
                        ['/develop/uniapp/myTimeLine', 'myTimeLine时间轴'],
                        ['/develop/uniapp/myWaterfall', 'myWaterfall瀑布流'],
                        ['/develop/uniapp/myWave', 'myWave波纹动态边框'],
                        ['/develop/uniapp/myOpenType', 'myOpenType开放类型'],
                        ['/develop/uniapp/myAd', 'myAd流量主广告'],
                        ['/develop/uniapp/myXcxAd', 'myXcxAd小程序广告'],
                        ['/develop/uniapp/myGap', 'myGap间隔'],
                        ['/develop/uniapp/vi', 'vi圆点分隔'],
                        ['/develop/uniapp/myPoster', 'myPoster海报'],
                        ['/develop/uniapp/chatFooter', 'chatFooter聊天框底部'],
                        ['/develop/uniapp/chatItem', 'chatItem聊天项'],
                    ]
                }


            ],


        },
        logo: '/common/logo.jpg',
        // 需要显示H5预览的地址集合
        simulatorUrl: []
    }
}
