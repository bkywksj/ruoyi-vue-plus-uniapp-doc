### 联系我

::: tip 温馨提示
如果你也想使用本框架快速开发 , 欢迎联系我进行授权购买
<qq-group></qq-group>
:::


### 作者


<team-member-item v-for="(item, index) in memberList" :key="index" :list="item"></team-member-item>


<script>
	export default {
		data() {
			return {
				memberList: [
					{
						avatar: '/common/zws.jpg',
						name: '抓蛙师',
						job: '全栈开发',
						addr: '广东',
						csdn: 'https://space.bilibili.com/520725002',
						duty: '负责项目管理、测试、兼容处理、文档管理、项目维护、升级等相关工作',
						intro: '多个项目开发经验，技术栈：java，vue，uniapp等',
                        github: 'https://gitee.com/bkywksj',
                        gitee: 'https://gitee.com/bkywksj'
					},
				],
                
			}
		}
	}
</script>


<style scoped>
.page {
	width: 500px;
}

.col-box {
	text-align: center;
}

</style>

