## myShops商铺

::: tip 提示
myShops组件是商铺列表展示组件,点击可以跳转到商店详情页面,不过商店详情页面需要增加并完善/subpages/index/shop/shop
:::

### 基本使用

```vue

<template>
  <view>
    <myShops :item="shopInfo"></myShops>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        shopInfo: {
          shopLogo: 'chat',
          shopName: '商店名称',
          score: 4.5,
          monthSale: 100,
          label: '好吃,好看',
          goodsList: [{
            goodsName: '商品1',
            mainImg: 'photo',
            price: '10'
          }, {
            goodsName: '商品2',
            mainImg: 'photo',
            price: '10'
          }]
        },
      }
    }
  }
</script>
```

<imgPreview src="/develop/uniapp/myShops1.png" :width="630" :height="456" style="margin-top: 20px"/>

### Props属性

| 参数   | 说明         | 类型     | 默认值 |   
|:-----|:-----------|:-------|:----|
| item | 商店和商店的商品信息 | Object | {}  |

### item属性

| 参数        | 说明                           | 类型     | 默认值 |   
|:----------|:-----------------------------|:-------|:----|
| shopLogo  | 商店logo                       | String | ''  |
| shopName  | 商店名称                         | String | ''  |
| score     | 评分                           | String | ''  |
| monthSale | 月销                           | String | ''  |
| label     | 标签                           | String | {}  |
| goodsList | 商品信息 goodsName mainImg price | Object | {}  |

