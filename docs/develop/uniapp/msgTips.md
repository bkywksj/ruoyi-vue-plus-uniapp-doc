## msgTips消息提示

::: tip 提示
msgTips组件是聊天消息提示弹窗, 默认显示5秒后消失. 调用this.$setOtherChat({nickname: '昵称', content: '你好',avatar:'
xxx,contentType:'text',uid:'xxx',type:'xxx'})
:::

### 基本使用

<p>在需要显示新聊天消息提示的页面中引入组件即可</p>

```vue

<template>
  <view>
    <msgTips></msgTips>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/msgTips1.png" :width="640" :height="273" style="margin-top: 20px"/>

### Props属性

| 参数   | 说明                      | 类型      | 默认值   |   
|:-----|:------------------------|:--------|:------|
| chat | 是否在聊天界面(在聊天界面可以不显示消息弹窗) | Boolean | false |

### this.$setOtherChat里面参数的otherChat属性

| 参数          | 说明                         |
|:------------|:---------------------------|
| nickname    | 昵称                         |
| avatar      | 头像                         |
| content     | 消息内容                       |
| contentType | 消息类型 image file video text |
| uid         | 用户id                       |
| type        | 消息来源类型标记                   |

