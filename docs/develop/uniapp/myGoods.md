## myGoods商品

::: tip 提示
myGoods组件是商品组件，主要用来展示商品信息。
:::

### 基本使用

```vue

<template>
  <view>
    <myGoods :item="{goodsName: '商品4',
            mainImg: 'photo',
            price: '10',
            shopName:'店铺',
            shopId:'1',
            goodsId:'1'}"></myGoods>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myGoods1.png" :width="635" :height="284" style="margin-top: 20px"/>

### Props属性

| 参数   | 说明   | 类型     | 默认值 |   
|:-----|:-----|:-------|:----|
| item | 商品详情 | Object | {}  |

### item属性

| 参数        | 说明   | 类型            | 默认值 |   
|:----------|:-----|:--------------|:----|
| mainImg   | 商品图片 | String        | ''  |
| goodsName | 商品名称 | String        | ''  |
| score     | 商品评分 | Number/String | ''  |
| price     | 商品价格 | Number/String | ''  |
| monthSale | 商品月销 | Number/String | 0   |
| shopName  | 商店名称 | String        | ''  |
| goodsId   | 商品id | String        | ''  |
| shopId    | 商店id | String        | ''  |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
