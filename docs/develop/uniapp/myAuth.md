## myAuth授权

::: tip 提示
myAuth组件是小程序内的头像昵称授权组件 v-model可以控制打开与关闭
:::

### 基本使用

```vue

<template>
  <view>
    <myAuth v-model="showAuth"></myAuth>

    <myBtn title="打开授权" @confirm="showAuth=true"></myBtn>

  </view>
</template>
<script>
  export default {
    data() {
      return {
        showAuth: false
      }
    }
  }
</script>

```

<imgPreview src="/develop/uniapp/myAuth1.png" :width="617" :height="137" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myAuth2.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数       | 说明    | 类型      | 默认值   |   
|:---------|:------|:--------|:------|
| value    | 双向绑定  | Boolean | false |
| required | 是否必填  | Boolean | false |
| tip      | tip提示 | String  | ''    |

### Event事件

| 事件名     | 说明     | 回调参数 | 
|:--------|:-------|:-----|
| hasAuth | 授权成功触发 | 无    |
