## formCertUpload证件上传

::: tip 提示
formCertUpload组件是身份证或者银行卡上传组件,支持正反面
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <!-- 身份证正反面 -->
        <myView justify-content="space-between">
          <formCertUpload label="身份证正面" v-model="form.sfzzm"></formCertUpload>
          <formCertUpload label="身份证反面" v-model="form.sfzfm" bg="/static/component/formCertUpload/sfzfm.png">
          </formCertUpload>
        </myView>
        <!-- 银行卡正反面 -->
        <myView justify-content="space-between">
          <formCertUpload label="银行卡正面" v-model="form.yhkzm" bg="/static/component/formCertUpload/yhkzm.png">
          </formCertUpload>
          <formCertUpload label="银行卡反面" v-model="form.yhkfm" bg="/static/component/formCertUpload/yhkzm.png">
          </formCertUpload>
        </myView>
      </u-form>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/formCertUpload1.png" :width="628" :height="542" style="margin-top: 20px"/>

### Props属性

| 参数         | 说明     | 类型            | 默认值                                          |   
|:-----------|:-------|:--------------|:---------------------------------------------|
| value      | 双向绑定的值 | String        | ''                                           |
| label      | 标签     | String        | '选择图片'                                       |
| bg         | 背景图片   | String        | '/static/component/formCertUpload/sfzzm.png' |
| prop       | 校验属性   | String        | ''                                           |
| background | 背景色    | String        | '#f7f7f7'                                    |
| marginTop  | 默认距离顶部 | Number\String | ''                                           |
| width      | 默认宽度   | Number\String | '296'                                        |
| height     | 默认高度   | Number\String | '222'                                        |

