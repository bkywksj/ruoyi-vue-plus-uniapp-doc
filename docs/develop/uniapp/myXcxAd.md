## myXcxAd小程序广告

::: tip 提示
myXcxAd组件是自定义的广告组件
:::

### 基本使用

```vue

<template>
  <view>
    <myXcxAd
        :adList="[{img:'/static/me/bg.png',title:'标题1',intro:'介绍1',appid:'xxx'},{img:'/static/me/bg.png',title:'标题2',intro:'介绍2',appid:'xxx'}]">
    </myXcxAd>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myXcxAd1.png" :width="640" :height="382" style="margin-top: 20px"/>

### Props属性

| 参数         | 说明               | 类型            | 默认值                                                   |   
|:-----------|:-----------------|:--------------|:------------------------------------------------------|
| type       | 提供了则会自动加载后端配置的广告 | String        | ''                                                    |
| marginTop  | 默认距离顶部           | Number\String | ''                                                    |
| background | 背景色              | String        | 'linear-gradient(to right, #fef9f5 0%, #ffece8 100%)' |
| color      | 去看看文字和边框的颜色      | String        | '#f67683'                                             |
| adList     | 非自动加载则传入广告数据     | Array         | []                                                    |

