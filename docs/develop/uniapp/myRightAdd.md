## myRightAdd右侧添加

::: tip 提示
myRightAdd组件是右下角添加按钮组件
:::

### 基本使用

```vue

<template>
  <view>
    <myRightAdd @confirm="confirm"></myRightAdd>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myRightAdd1.png" :width="619" :height="854" style="margin-top: 20px"/>

### Props属性

| 参数       | 说明   | 类型            | 默认值                                    |   
|:---------|:-----|:--------------|:---------------------------------------|
| src      | 图标   | String        | '/static/component/myRightAdd/add.png' |
| position | 相对位置 | String        | ''                                     |
| bottom   | 距离底部 | Number\String | '160'                                  |
| right    | 距离右侧 | Number\String | '16'                                   |
| width    | 默认宽度 | Number\String | '144'                                  |
| height   | 默认高度 | Number\String | '144'                                  |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
