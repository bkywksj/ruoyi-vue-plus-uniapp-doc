## mySearchHistory搜索历史

::: tip 提示
mySearchHistory组件是搜索历史组件,比如搜索商品记录等,可以设置到缓存里面进行读取
:::

### 基本使用

```vue

<template>
  <view>
    <mySearchHistory cacheKey="goodsSearchHistory" :values="['历史1','历史2']" red="mySearchHistory"></mySearchHistory>
  </view>
</template>
<script>
  export default {
    methods: {
      addHistory() {
        this.$refs.mySearchHistory.addHistory('历史3')
      },
    }
  }
</script>

```

<imgPreview src="/develop/uniapp/mySearchHistory1.png" :width="620" :height="166" style="margin-top: 20px"/>

### Props属性

| 参数       | 说明       | 类型      | 默认值    |   
|:---------|:---------|:--------|:-------|
| showDel  | 是否显示删除按钮 | Boolean | true   |
| values   | 初始值      | Array   | []     |
| title    | 标题       | String  | '搜索历史' |
| cacheKey | 缓存key    | String  | ''     |
| maxSize  | 最多缓存数量   | Number  | 10     |

### Event事件

| 事件名    | 说明    | 回调参数 | 
|:-------|:------|:-----|
| choose | 选择时触发 | item |
