## formSwitch开关

::: tip 提示
formSwitch组件是表单开关组件
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formSwitch label="是否开启" v-model="form.open"></formSwitch>
        <formSwitch label="是否开启2" activeValue="10" inactiveValue="20" v-model="form.open2" @change="change">
        </formSwitch>
      </u-form>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/formSwitch1.png" :width="614" :height="289" style="margin-top: 20px"/>

### Props属性

| 参数            | 说明        | 类型            | 默认值       |   
|:--------------|:----------|:--------------|:----------|
| value         | 双向绑定的值    | String\Number | ''        |
| label         | 标签        | String        | ''        |
| labelColor    | 标签颜色      | String        | '#333333' |
| required      | 是否必填      | Boolean       | false     |
| borderBottom  | 是否显示下划线   | Boolean       | true      |
| prop          | 校验属性      | String        | ''        |
| marginTop     | 默认距离顶部    | Number\String | '0'       |
| activeValue   | 默认激活值 '1' | String        | '1'       |
| inactiveValue | 非激活值 '0'  | String        | '0'       |

### Event事件

| 事件名    | 说明    | 回调参数 | 
|:-------|:------|:-----|
| change | 切换时触发 | e    |
