## myMask遮罩

::: tip 提示
myMask组件是遮罩组件
:::

### 基本使用

```vue

<template>
  <view>
    <myMask v-model="open">
      <myView>
        <!--   内容     -->
      </myView>
    </myMask>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myMask1.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明        | 类型            | 默认值      |   
|:---------------|:----------|:--------------|:---------|
| value          | 双向绑定      | Boolean       | false    |
| opacity        | 透明度       | String        | '0.6'    |
| zIndex         | 层级        | Number\String | '9999'   |
| justifyContent | 对齐        | String        | 'center' |
| alignItems     | 对齐        | String        | 'center' |
| duration       | 间隔 延迟关闭 秒 | Number        | 0        |

### Event事件

| 事件名   | 说明   | 回调参数 | 
|:------|:-----|:-----|
| close | 关闭触发 | 无    |
