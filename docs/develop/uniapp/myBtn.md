## myBtn按钮

::: tip 提示
myBtn是按钮组件,方便控制内容,位置,确认提示等
:::

### 基本使用

```vue

<template>
  <view>
    <myCard display="flex" col>
      <myBtn></myBtn>
      <myBtn marginTop="32" title="这是按钮" width="200" height="60" plain type="primary" shape="circle"></myBtn>
      <myBtn marginTop="32" title="这是按钮2" width="200" height="60" type="error" icon="chat"></myBtn>
      <myBtn marginTop="32" title="这是按钮3" width="200" height="60" type="warning" showConfirm tip="确认点击吗?"
             @confirm="confirm"></myBtn>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myBtn1.png" :width="624" :height="404" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明                                           | 类型            | 默认值       |   
|:---------------|:---------------------------------------------|:--------------|:----------|
| title          | 按钮文字                                         | String        | '按钮'      |
| top            | 距离顶部                                         | String        | ''        |
| boxShadow      | 阴影效果                                         | String        | ''        |
| right          | 距离右侧                                         | String        | ''        |
| bottom         | 下边                                           | String        | ''        |
| left           | 左边                                           | String        | ''        |
| position       | 位置                                           | String        | ''        |
| type           | 类型 primary / success / info/ warning / error | String        | ''        |
| shape          | 形状 sqaure circle                             | String        | ''        |
| size           | 按钮大小 default medium  mini                    | String        | 'default' |
| disabled       | 是否禁用                                         | Boolean       | false     |
| plain          | 是否平面                                         | Boolean       | false     |
| ripple         | 点击是否水波纹                                      | Boolean       | false     |
| tip            | 点击确认提示                                       | String        | ''        |
| icon           | 图标                                           | String        | '-'       |
| titleSize      | 标题大小                                         | Number\String | '28'      |
| alignItems     | 对齐                                           | String        | 'center'  |
| justifyContent | 对齐                                           | String        | 'center'  |
| marginTop      | 距离上边                                         | Number\String | ''        |
| marginRight    | 距离右侧                                         | Number\String | ''        |
| marginBottom   | 底边距                                          | Number\String | ''        |
| marginLeft     | 默认距离左侧                                       | Number\String | ''        |
| display        | 布局                                           | String        | 'flex'    |
| bgWidth        | 背景宽度                                         | String        | ''        |
| width          | 宽度                                           | String        | ''        |
| height         | 高度                                           | String        | ''        |
| borderRadius   | 圆角                                           | Number\String | ''        |
| background     | 按钮背景                                         | String        | ''        |
| showConfirm    | 是否显示确认提示                                     | Boolean       | false     |
| customStyle    | 自定义样式                                        | Object        | {}        |
| showBorder     | 是否显示按钮边框                                     | Boolean       | true      |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
