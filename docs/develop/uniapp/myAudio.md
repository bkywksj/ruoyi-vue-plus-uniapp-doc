## myAudio音频

::: tip 提示
myAudio组件是音频播放组件,支持自动播放,可以设置播放图标位置在左侧或者右侧,可用于聊天中的语音播放
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer background="#999999">
      <myAudio src="xxxx.mp3"></myAudio>
    </myContainer>
    <myContainer background="#999999">
      <myAudio src="xxxx.mp3" isLeft></myAudio>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myAudio1.png" :width="640" :height="502" style="margin-top: 20px"/>

### Props属性

| 参数       | 说明        | 类型      | 默认值   |   
|:---------|:----------|:--------|:------|
| autoPlay | 是否自动播放    | Boolean | false |
| src      | 音频地址      | String  | ''    |
| isLeft   | 播放图标是否在左侧 | Boolean | false |
