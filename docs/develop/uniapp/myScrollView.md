## myScrollView可滑动view

::: tip 提示
myScrollView组件是uniapp中比较基础的组件.可以实现x轴/y轴的滚动.
:::

### 基本使用

```vue

<template>
  <view>
    <myScrollView height="300" scrollY>
      <myView v-for="(item,index) in 10" :key="index" background="red" marginBottom="10" width="100" height="100">
      </myView>
    </myScrollView>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myScrollView1.gif" :width="103" :height="273" style="margin-top: 20px"/>

### Props属性

| 参数                    | 说明                                                                                            | 类型            | 默认值     |   
|:----------------------|:----------------------------------------------------------------------------------------------|:--------------|:--------|
| scrollX               | 允许横向滚动                                                                                        | Boolean       | false   |
| scrollY               | 允许纵向滚动                                                                                        | Boolean       | false   |
| upperThreshold        | 距顶部/左边多远时（单位px），触发 scrolltoupper 事件                                                           | Number\String | 50      |
| lowerThreshold        | 距底部/右边多远时（单位px），触发 scrolltolower 事件                                                           | Number\String | 50      |
| scrollTop             | 设置竖向滚动条位置                                                                                     | Number\String | ''      |
| scrollLeft            | 设置横向滚动条位置                                                                                     | Number\String | ''      |
| scrollIntoView        | 值应为某子元素id（id不能以数字开头）。设置哪个方向可滚动，则在哪个方向滚动到该元素                                                   | String        | ''      |
| scrollWithAnimation   | 在设置滚动条位置时使用动画过渡                                                                               | Boolean       | false   |
| enableBackToTop       | iOS点击顶部状态栏、安卓双击标题栏时，滚动条返回顶部，只支持竖向 app-nvue，微信小程序                                              | Boolean       | false   |
| showScrollbar         | 控制是否出现滚动条 App-nvue 2.1.5+                                                                     | Boolean       | false   |
| refresherEnabled      | 开启自定义下拉刷新                                                                                     | Boolean       | false   |
| refresherThreshold    | 设置自定义下拉刷新阈值                                                                                   | Number\String | 45      |
| refresherDefaultStyle | 设置自定义下拉刷新默认样式，支持设置 black，white，none，none 表示不使用默认样式                                            | String        | 'black' |
| refresherBackground   | 设置自定义下拉刷新区域背景颜色                                                                               | String        | '#FFF'  |
| refresherTriggered    | 设置当前下拉刷新状态，true 表示下拉刷新已经被触发，false 表示下拉刷新未被触发                                                  | 				Boolean   | false   |
| enableFlex            | 启用 flexbox 布局。开启后，当前节点声明了 display: flex 就会成为 flex container，并作用于其孩子节点。 微信小程序 2.7.3            | Boolean       | false   |
| scrollAnchoring       | 开启 scroll anchoring 特性，即控制滚动位置不随内容变化而抖动，仅在 iOS 下生效，安卓下可参考 CSS overflow-anchor 属性。	微信小程序 2.8.2 | Boolean       | false   |
| width                 | scrollX为真的时候需要设置宽度                                                                            | Number\String | ''      |
| height                | scrollY为真的时候需要设置高度                                                                            | Number\String | ''      |
| margin                | 外边距                                                                                           | Number\String | ''      |
| marginLeft            | 距离左侧                                                                                          | Number\String | ''      |
| marginRight           | 距离右侧                                                                                          | Number\String | ''      |
| marginTop             | 距离顶部                                                                                          | Number\String | ''      |
| marginBottom          | 距离下边                                                                                          | Number\String | ''      |
| background            | 背景                                                                                            | String        | ''      |
| customStyle           | 自定义样式                                                                                         | Object        | {}      |

### Event事件

| 事件名              | 说明                                                                                      | 回调参数 | 
|:-----------------|:----------------------------------------------------------------------------------------|:-----|
| scrolltoupper    | 滚动到顶部/左边，会触发 scrolltoupper 事件                                                           | 无    |
| scrolltolower    | 滚动到底部/右边，会触发 scrolltolower 事件                                                           | 无    |
| scroll           | 滚动时触发，event.detail = {scrollLeft, scrollTop, scrollHeight, scrollWidth, deltaX, deltaY} | 无    |
| refresherpulling | 自定义下拉刷新控件被下拉	H5、app-vue 2.5.12+,微信小程序基础库2.10.1+                                         | 无    |
| refresherrefresh | 自定义下拉刷新被触发	H5、app-vue 2.5.12+,微信小程序基础库2.10.1+                                           | 无    |
| refresherrestore | 自定义下拉刷新被复位	H5、app-vue 2.5.12+,微信小程序基础库2.10.1+                                           | 无    |
| refresherabort   | 自定义下拉刷新被中止                                                                              | 无    |
| confirm          | 点击滚动栏                                                                                   | 无    |
