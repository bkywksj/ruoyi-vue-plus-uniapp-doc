## myViewButton视图按钮

::: tip 提示
myViewButton是标签按钮组件
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myViewButton label="标签1"></myViewButton>
      <myViewButton type="primary" label="标签2"></myViewButton>
      <myViewButton type="warning" label="标签3"></myViewButton>
      <myViewButton type="info" label="标签4"></myViewButton>
      <myViewButton type="error" label="标签5"></myViewButton>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myViewButton1.png" :width="631" :height="143" style="margin-top: 20px"/>

### Props属性

| 参数     | 说明                            | 类型      | 默认值     |   
|:-------|:------------------------------|:--------|:--------|
| type   | 类型 info primary warning error | String  | 'info'  |
| label  | 标签                            | String  | '标签'    |
| size   | 大小                            | String  | '28'    |
| square | 是否方形                          | Boolean | false   |
| width  | 宽度                            | String  | ''      |
| height | 高度                            | String  | ''      |
| color  | 文字颜色                          | String  | 'white' |
