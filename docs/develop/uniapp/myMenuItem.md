## myMenuItem菜单项

::: tip 提示
myMenuItem组件是是菜单项
:::

### 基本使用

```vue

<template>
  <view>
    <myView col background="white" padding="0 20rpx">
      <myMenuItem title="我的消息" icon="chat" :num="2" @confirm="toMyMsg">
      </myMenuItem>
      <myMenuItem title="我的消息2" icon="chat" :num="3" @confirm="toMyMsg">
      </myMenuItem>
    </myView>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myMenuItem1.png" :width="631" :height="268" style="margin-top: 20px"/>

### Props属性

| 参数            | 说明     | 类型            | 默认值           |   
|:--------------|:-------|:--------------|:--------------|
| title         | 标题     | String        | ''            |
| position      | 相对位置   | String        | ''            |
| width         | 默认宽度   | Number\String | ''            |
| marginLeft    | 默认距离左侧 | Number\String | ''            |
| padding       | 默认内边距  | Number\String | ''            |
| borderRadius  | 默认圆角   | Number\String | ''            |
| icon          | 图标     | String        | ''            |
| num           | 未读数量   | Number\String | 0             |
| overflowCount | 上限未读数量 | Number\String | 99            |
| rightLabel    | 右侧标签   | String        | ''            |
| rightIcon     | 右侧图标   | String        | 'arrow-right' |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
