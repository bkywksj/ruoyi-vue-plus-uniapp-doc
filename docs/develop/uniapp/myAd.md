## myAd流量主广告

::: tip 提示
myAd组件是流量主广告封装组件,方便进行广告接入
:::

### 基本使用

```vue

<template>
  <view>
    <!-- //微信小程序各种广告引入示例 -->
    <!-- banner广告使用 -->
    <myAd type="0" @load="load" @error="error" @close="close"></myAd>
    <!-- //激励视频广告使用 -->
    <myAd type="1" v-model="showRewardAd" @close="close"></myAd>
    <u-button size="medium" @click="showRewardAd=true">打开激励视频广告</u-button>
    <!-- //插屏广告使用 -->
    <myAd type="2" v-model="showInsertAd" @close="close"></myAd>
    <u-button size="medium" @click="showInsertAd=true">打开插屏广告</u-button>
    <!-- //视频广告 -->
    <myAd type="3" ad-theme="white" @load="load" @error="error" @close="close"></myAd>
    <!-- //视频贴片广告 -->
    <myAd type="4" src=""></myAd>
    <!-- //原生模板广告 -->
    <myAd type="5" @load="load" @error="error" @hide="hide"></myAd>

  </view>
</template>

```

<imgPreview src="/develop/uniapp/myAd1.png" :width="634" :height="293" style="margin-top: 20px"/>

### Props属性

| 参数           | 说明                                           | 类型            | 默认值            |   
|:-------------|:---------------------------------------------|:--------------|:---------------|
| type         | 0banner广告 1激励广告 2插屏广告 3视频广告 4视频贴片广告  5原生模板广告 | String        | ''             |
| value        | 控制激励视频广告 插屏广告的显示                             | Boolean       | false          |
| title        | 激励视频弹窗标题                                     | String        | '温馨提示'         |
| tip          | 激励视频弹窗提示                                     | String        | '获取奖励需要观看激励视频' |
| background   | 背景色                                          | Number\String | ''             |
| position     | 广告相对位置                                       | String        | 'relative'     |
| zIndex       | 广告层级                                         | String        | '9999'         |
| adTheme      | 广告主题 black white 两种                          | String        | 'white'        |
| adIntervals  | 广告切换间隔 必须大于等于30                              | Number        | 30             |
| src          | 视频广告视频播放链接                                   | String        | ''             |
| width        | 宽度                                           | Number\String | ''             |
| height       | 高度                                           | Number\String | ''             |
| margin       | 边距                                           | Number\String | ''             |
| marginTop    | 默认距离顶部                                       | Number\String | ''             |
| marginRight  | 右边距                                          | Number\String | ''             |
| marginBottom | 底边距                                          | Number\String | ''             |
| marginLeft   | 默认距离左侧                                       | Number\String | ''             |
| border       | 边框                                           | String        | ''             |
| borderRadius | 默认圆角                                         | Number\String | ''             |

### Event事件

| 事件名   | 说明          | 回调参数           | 
|:------|:------------|:---------------|
| close | 插屏广告/激励视频关闭 | isEnded 是否播放结束 |
| load  | 广告加载完成      | e              |
| error | 广告加载错误      | e              |
| hide  | 广告隐藏        | e              |
