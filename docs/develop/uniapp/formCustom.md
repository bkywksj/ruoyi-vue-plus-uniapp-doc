## formCustom自定义表单

::: tip 提示
formCustom组件是自定义表单,如果别的表单组件无法满足需要,可以使用此组件,通过插槽的方式实现更灵活的功能
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formCustom label="名称">
          <u-input v-model="form.name"></u-input>
          <u-button v-slot="right" size="mini">按钮</u-button>
        </formCustom>
        <formCustom label="名称2" right-icon="chat" :rightIconStyle="{fontSize:'50rpx'}">
          <u-input v-model="form.name2"></u-input>
        </formCustom>
      </u-form>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/formCustom1.png" :width="621" :height="416" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明      | 类型            | 默认值             |   
|:---------------|:--------|:--------------|:----------------|
| value          | 双向绑定的值  | String\Number | ''              |
| col            | 是否竖直方向  | Boolean       | true            |
| marginTop      | 默认距离顶部  | Number\String | '32'            |
| position       | 相对位置    | String        | ''              |
| prop           | 校验属性    | String        | ''              |
| label          | 标签      | String        | ''              |
| labelWidth     | 标签宽度    | Number\String | ''              |
| labelStyle     | 标签样式    | Object        | {}              |
| labelAlign     | 标签对齐方式  | String        | ''              |
| leftIcon       | 左侧图标    | String        | ''              |
| leftIconStyle  | 左侧图标样式  | Object        | {}              |
| rightIcon      | 右侧图标    | String        | ''              |
| rightIconStyle | 右侧图标样式  | Object        | {}              |
| borderBottom   | 是否显示下划线 | Boolean       | true            |
| required       | 是否必填    | Boolean       | false           |
| width          | String  | String        | 622             |
| justifyContent | 对齐      | String        | 'space-between' |
| alignItems     | 对齐      | String        | ''              |
