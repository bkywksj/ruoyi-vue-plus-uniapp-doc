## myTitle标题

::: tip 提示
myTitle是标题组件
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myTitle ltitle="主标题" showDot></myTitle>
      <myTitle ltitle="主标题" showBorder></myTitle>
      <myTitle ltitle="主标题" showLine></myTitle>
      <myTitle ltitle="主标题" licon="chat"></myTitle>
      <myTitle ltitle="主标题" licon="chat" ltitlecolor="red" tsize="40"></myTitle>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myTitle1.png" :width="631" :height="415" style="margin-top: 20px"/>

### Props属性

| 参数          | 说明       | 类型            | 默认值                                                                                      |   
|:------------|:---------|:--------------|:-----------------------------------------------------------------------------------------|
| showBorder  | 是否显示左侧竖线 | Boolean       | false                                                                                    |
| showDot     | 是否显示左侧点  | Boolean       | false                                                                                    |
| showLine    | 是否显示下划线  | Boolean       | false                                                                                    |
| bold        | 是否加粗     | Boolean       | true                                                                                     |
| marginTop   | 默认距离顶部   | Number\String | '32'                                                                                     |
| marginLeft  | 默认距离左侧   | Number\String | '32'                                                                                     |
| tsize       | 标题大小     | Number\String | '32'                                                                                     |
| ltitle      | 主标题      | String        | '主标题'                                                                                    |
| ltitlecolor | 主标题颜色    | String        | '#666666'                                                                                |
| lcolor      | 点线颜色     | String        | '#4db65c'                                                                                |
| licon       | 左侧图标     | String        | ''                                                                                       |
| liconWidth  | 左侧图标宽度   | Number\String | '40'                                                                                     |
| liconHeight | 左侧图标高度   | Number\String | '40'                                                                                     |
| background  | 背景色      | String        | 'linear-gradient(179deg, #11cb07 100%, #f9fbff 50%, transparent 100%, transparent 100%)' |

### Event事件

| 事件名      | 说明     | 回调参数 | 
|:---------|:-------|:-----|
| tapLeft  | 点击左侧触发 | 无    |
| tapRight | 点击右侧触发 | 无    |
