## myButton按钮

::: tip 提示
myButton是按钮组件,方便控制内容,位置,确认提示等,样式与myBtn有区别,且支持弹窗输入内容
:::

### 基本使用

```vue

<template>
  <view>
    <myCard display="flex" col>
      <myButton></myButton>
      <myButton width="200" marginTop="32" type="primary"></myButton>
      <myButton marginTop="32" type="ban" showConfirm @confirm="confirm1"></myButton>
      <myButton marginTop="32" type="error" showPop @confirm="confirm2"></myButton>
    </myCard>
  </view>
</template>
<script>
  export default {
    methods: {
      confirm1() {

      },
      confirm2(e) {
        console.log("弹窗输入值", e)
      },
    }
  }
</script>

```

<imgPreview src="/develop/uniapp/myButton1.png" :width="624" :height="404" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明                                           | 类型            | 默认值       |   
|:---------------|:---------------------------------------------|:--------------|:----------|
| type           | 按钮类型 primary ban error                       | String        | 'primary' |
| title          | 按钮文字                                         | String        | '按钮'      |
| titleSize      | 标题大小                                         | Number\String | '24'      |
| width          | 宽度                                           | Number\String | '136'     |
| height         | 高度                                           | Number\String | '64'      |
| icon           | 图标                                           | String        | '-'       |
| showConfirm    | 是否显示确认提示                                     | Boolean       | false     |
| showBorder     | 是否显示按钮边框                                     | Boolean       | true      |
| showPop        | 是否显示弹窗输入                                     | Boolean       | false     |
| marginTop      | 距离上边                                         | Number\String | ''        |
| marginRight    | 距离右侧                                         | Number\String | ''        |
| marginBottom   | 底边距                                          | Number\String | ''        |
| marginLeft     | 默认距离左侧                                       | Number\String | ''        |
| shape          | 形状 sqaure circle                             | String        | ''        |
| tip            | 点击确认提示                                       | String        | ''        |
| list           | 驳回需要                                         | Array         | []        |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
