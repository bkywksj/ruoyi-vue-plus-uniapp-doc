## chatItem聊天项

::: tip 提示
chatltem组件是聊天的聊天卡片
:::

### 基本使用

```vue

<template>
  <view>
    <!--  根据uid自动判断卡片位于左侧还是右侧  -->
    <chatItem :item="{avatar:'chat',uid:'',contentType:'chat',content:'内容'}"></chatItem>

    <chatItem isSelf :item="{avatar:'chat',uid:'xx',contentType:'chat',content:'内容2'}"></chatItem>

    <chatItem isSelf :item="{avatar:'chat',uid:'xx',contentType:'video',content:'内容2'}"></chatItem>

  </view>
</template>

```

<imgPreview src="/develop/uniapp/chatItem1.png" :width="640" :height="475" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明       | 类型      | 默认值  |   
|:---------------|:---------|:--------|:-----|
| item           | 聊天内容     | Object  | {}   |
| showReadStatus | 是否显示已读未读 | Boolean | true |

