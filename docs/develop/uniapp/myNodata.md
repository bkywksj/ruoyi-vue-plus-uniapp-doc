## myNodata无数据

::: tip 提示
myNodata组件无数据时显示的组件
:::

### 基本使用

```vue

<template>
  <view>
    <myNodata v-if="!hasData"></myNodata>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myNodata1.png" :width="574" :height="408" style="margin-top: 20px"/>

### Props属性

| 参数          | 说明      | 类型            | 默认值                                   |   
|:------------|:--------|:--------------|:--------------------------------------|
| src         | 图片地址    | String        | '/static/component/myPage/nodata.png' |
| tip         | 提示      | String        | '暂无数据'                                |
| width       | 宽度      | Number\String | '300'                                 |
| height      | 高度      | Number\String | '300'                                 |
| marginTop   | 默认距离顶部  | Number\String | '0'                                   |
| btnTitle    | 按钮标题    | String        | ''                                    |
| customStyle | 按钮自定义属性 | Object        | {}                                    |

### Event事件

| 事件名     | 说明      | 回调参数 | 
|:--------|:--------|:-----|
| confirm | 点击按钮时触发 | 无    |
