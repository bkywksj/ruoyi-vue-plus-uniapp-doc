## myTimeLine时间轴

::: tip 提示
myTimeLine组件是时间轴组件
:::

### 基本使用

```vue

<template>
  <view>
    <myTimeLine :list="[{icon:'chat',title:'标题1',content:'内容1'},{icon:'photo',title:'标题2',content:'内容2'}]">
    </myTimeLine>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myTimeLine1.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明       | 类型            | 默认值     |   
|:---------------|:---------|:--------------|:--------|
| list            | 时间轴内容    | Array          | []  |
| marginTop       | 距离顶部     | String        | ''      |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
