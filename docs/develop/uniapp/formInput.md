## formInput输入框

::: tip 提示
formInput组件是表单输入组件
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formInput label="名称" v-model="form.name"></formInput>
        <formInput col label="名称2" v-model="form.name2"></formInput>
        <formInput col mode="tel" label="电话" v-model="form.tel"></formInput>
        <formInput col mode="code" label="电话2" v-model="form.tel2"></formInput>
        <formInput col mode="map" label="位置" v-model="form.position"></formInput>
      </u-form>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/formInput1.png" :width="624" :height="792" style="margin-top: 20px"/>

### Props属性

| 参数               | 说明                                               | 类型            | 默认值             |   
|:-----------------|:-------------------------------------------------|:--------------|:----------------|
| value            | 双向绑定的值                                           | String\Number | ''              |
| col              | 是否竖直方向                                           | Boolean       | true            |
| width            | 表单宽度                                             | Number\String | ''              |
| marginTop        | 默认距离顶部                                           | Number\String | '0'             |
| position         | 相对位置                                             | String        | ''              |
| prop             | 校验属性                                             | String        | ''              |
| label            | 标签                                               | String        | ''              |
| labelWidth       | 标签宽度                                             | Number\String | ''              |
| labelStyle       | 标签样式                                             | Object        | {}              |
| labelAlign       | 标签对齐方式                                           | String        | ''              |
| leftIcon         | 左侧图标                                             | String        | ''              |
| leftIconStyle    | 左侧图标样式                                           | Object        | {}              |
| rightIcon        | 右侧图标                                             | String        | ''              |
| rightIconStyle   | 右侧图标样式                                           | Object        | {}              |
| borderBottom     | 是否显示下划线                                          | Boolean       | true            |
| required         | 是否必填                                             | Boolean       | false           |
| tip              | 提示                                               | String        | ''              |
| show             | textarea 在弹窗里面内容填充颜色问题兼容处理                       | Boolean       | false           |
| clearable        | 是否带清楚按钮                                          | Boolean       | false           |
| type             | 类型 'text' /select / password / textarea / number | String        | 'text'          |
| border           | 边框                                               | Boolean       | false           |
| inputStyle       | 输入样式                                             | Object        | {}              |
| inputAlign       | 输入框对齐默认空/center/right                            | String        | ''              |
| placeholder      | 填充值                                              | String        | ''              |
| placeholderStyle | 填充样式                                             | String        | 'color:#CACFDB' |
| adjustPosition   | 是否调整位置                                           | Boolean       | true            |
| maxlength        | 最大长度                                             | Number\String | 200             |
| disabled         | 是否禁用                                             | Boolean       | false           |
| mode             | 模式 tel code map                                  | String        | ''              |
| focus            | 是否聚焦                                             | Boolean       | false           |

### Event事件

| 事件名            | 说明                    | 回调参数 | 
|:---------------|:----------------------|:-----|
| input          | 输入时触发                 | e    |
| focus          | 聚焦时触发                 | 无    |
| blur           | 失去焦点时触发               | 无    |
| chooseLocation | mode为map时选择位置触发       | 无    |
| getTel         | 小程序内模式为tel时候点击获取手机号触发 | e    |
