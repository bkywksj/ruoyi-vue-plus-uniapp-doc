## formImgUpload图片上传

::: tip 提示
formImgUpload组件是图片上传组件
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formImgUpload label="图片" v-model="form.imgUrl"></formImgUpload>
        <formImgUpload label="图片2" leftIcon="photo" tip="限制大小2MB" v-model="form.imgUrl2"></formImgUpload>
      </u-form>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/formImgUpload1.png" :width="623" :height="662" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明      | 类型            | 默认值    |   
|:---------------|:--------|:--------------|:-------|
| value          | 双向绑定的值  | String\Number | ''     |
| col            | 是否竖直方向  | Boolean       | true   |
| marginTop      | 默认距离顶部  | Number\String | '0'    |
| position       | 相对位置    | String        | ''     |
| prop           | 校验属性    | String        | ''     |
| label          | 标签      | String        | ''     |
| labelWidth     | 标签宽度    | Number\String | ''     |
| labelStyle     | 标签样式    | Object        | {}     |
| labelAlign     | 标签对齐方式  | String        | ''     |
| leftIcon       | 左侧图标    | String        | ''     |
| leftIconStyle  | 左侧图标样式  | Object        | {}     |
| rightIcon      | 右侧图标    | String        | ''     |
| rightIconStyle | 右侧图标样式  | Object        | {}     |
| borderBottom   | 是否显示下划线 | Boolean       | true   |
| required       | 是否必填    | Boolean       | false  |
| tip            | 提示      | String        | ''     |
| uploadText     | 上传文字提示  | String        | '添加文件' |
| width          | 默认宽度    | Number\String | 184    |
| height         | 默认高度    | Number\String | 184    |
| maxSize        | 最大大小    | Number        | 2      |
| maxCount       | 最大数量    | Number        | 1      |

