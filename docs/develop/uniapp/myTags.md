## myTags标签

::: tip 提示
myTags组件是标签组件,可以控制样式,还可以控制选中取消状态
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <myTags value="苹果,哈密瓜"></myTags>
      <myTags value="苹果,哈密瓜" type="primary" marginTop="20"></myTags>
      <myTags value="苹果,哈密瓜" type="success" marginTop="20"></myTags>
      <myTags value="苹果,哈密瓜" type="warning" marginTop="20"></myTags>
      <myTags value="苹果,哈密瓜" type="error" marginTop="20"></myTags>
      <myTags value="苹果,哈密瓜" type="choose" :current="current" marginTop="20" @confirm="confirmTag"></myTags>

      <myTags value="1" :dicts="[{label:'西瓜',value:'1'},{label:'梨',value:'2'}]" type="error" marginTop="20">
      </myTags>
    </myContainer>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        current: 0,
      }
    },
    methods: {
      confirmTag(item, index) {
        this.current = index
      }
    }
  }
</script>
```

<imgPreview src="/develop/uniapp/myTags1.png" :width="625" :height="484" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明                           | 类型            | 默认值          |   
|:---------------|:-----------------------------|:--------------|:-------------|
| value          | 绑定值,逗号隔开                     | String        | ''           |
| type           | 标签类型,如果提供字典,回根据字典提供的type优先显示 | String        | 'info'       |
| dicts          | 字典值                          | Array         | []           |
| size           | 标签大小                         | String        | '24'         |
| marginTop      | 距离顶部                         | String        | ''           |
| marginLeft     | 距离左侧                         | String        | ''           |
| flexWrap       | 换行规则                         | String        | 'wrap'       |
| borderRadius   | 圆角                           | String        | '8'          |
| marginRight    | 距离右侧                         | String        | ''           |
| padding        | 内边距                          | Number\String | '6rpx 12rpx' |
| tagMarginTop   | 标签距离顶部                       | String        | ''           |
| tagMarginRight | 标签距离右侧                       | String        | ''           |
| color          | 颜色                           | String        | ''           |
| current        | 当前激活索引                       | Number        | -1           |

### Event事件

| 事件名     | 说明      | 回调参数        | 
|:--------|:--------|:------------|
| confirm | 点击标签时触发 | item, index |
