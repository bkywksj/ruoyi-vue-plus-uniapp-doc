## myCartGoods购物车商品

::: tip 提示
myCartGoods组件是购物车商品组件,用于购物车页面显示已添加购物车商品
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myCartGoods showCheckbox :value="{goodsName:'商品',mainImg:'chat',isSelect:true,price:'12'}"></myCartGoods>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myCartGoods1.png" :width="624" :height="279" style="margin-top: 20px"/>

### Props属性

| 参数           | 说明      | 类型      | 默认值   |   
|:-------------|:--------|:--------|:------|
| value        | 双向绑定    | Object  | {}    |
| showCheckbox | 是否显示选择框 | Boolean | false |

### Event事件

| 事件名    | 说明       | 回调参数     | 
|:-------|:---------|:---------|
| change | 勾选取消勾选触发 | isSelect |
