## myMenu菜单

::: tip 提示
myMenu组件菜单组件
:::

### 基本使用

```vue

<template>
  <view>
    <myMenu :tabbar="tabbar" @choose="choose" @toDetail="toDetail"></myMenu>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        tabbar: [{
          name: '日常用品',
          goodsList: [{
            goodsName: '商品1',
            mainImg: 'photo',
            price: '10'
          }, {
            goodsName: '商品2',
            mainImg: 'photo',
            price: '10'
          }]
        }, {
          name: '电脑器件',
          goodsList: [{
            goodsName: '商品3',
            mainImg: 'photo',
            price: '10'
          }, {
            goodsName: '商品4',
            mainImg: 'photo',
            price: '10'
          }]
        }],
      }
    },
    methods: {
      choose(item) {
      },
      toDetail(item) {
      }
    }
  }
</script>
```

<imgPreview src="/develop/uniapp/myMenu1.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数     | 说明      | 类型    | 默认值 |   
|:-------|:--------|:------|:----|
| tabbar | 分类和商品信息 | Array | []  |

### tabbar每一项的属性

| 参数        | 说明                           | 类型     | 默认值 |   
|:----------|:-----------------------------|:-------|:----|
| name      | 分类名称                         | String | ''  |
| goodsList | 商品信息 goodsName mainImg price | Object | {}  |

