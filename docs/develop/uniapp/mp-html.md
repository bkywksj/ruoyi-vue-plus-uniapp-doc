## mp-html富文本展示

::: tip 提示
mp-html组件是富文本回显组件
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formMp label="内容" v-model="form.content"></formMp>
      </u-form>

      <myView marginTop="100">
        <mp-html :content="form.content"></mp-html>
      </myView>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/mp-html1.png" :width="624" :height="499" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明                      | 类型             | 默认值   |   
|:---------------|:------------------------|:---------------|:------|
| containerStyle | 容器的样式                   | String         | ''    |
| content        | 用于渲染的 html 字符串          | String         | ''    |
| copyLink       | 是否允许外部链接被点击时自动复制        | Boolean        | true  |
| domain         | 主域名，用于拼接链接              | String         | ''    |
| errorImg       | 图片出错时的占位图链接             | String         | ''    |
| lazyLoad       | 是否开启图片懒加载               | Boolean        | false |
| loadingImg     | 图片加载过程中的占位图链接           | String         | ''    |
| pauseVideo     | 是否在播放一个视频时自动暂停其他视频      | Boolean        | true  |
| previewImg     | 是否允许图片被点击时自动预览          | Boolean        | true  |
| scrollTable    | 是否给每个表格添加一个滚动层使其能单独横向滚动 | Boolean        | false |
| selectable     | 是否开启长按复制                | Boolean        | false |
| setTitle       | 是否将 title 标签的内容设置到页面标题  | Boolean/String | true  |
| showImgMenu    | 是否允许图片被长按时显示菜单          | Boolean/String | true  |
| tagStyle       | 标签的默认样式                 | Object         | {}    |
| useAnchor      | 是否使用锚点链接                | Boolean/Number | false |

### Event事件

| 事件名     | 说明            | 回调参数 | 
|:--------|:--------------|:-----|
| load    | dom 结构加载完毕时触发 | 无    | 
| ready   | 所有图片加载完毕时触发   | e    | 
| imgTap  | 图片被点击时触发      | e    | 
| linkTap | 链接被点击时触发      | e    | 
| error   | 媒体加载出错时触发     | e    | 
