## vi圆点分隔

::: tip 提示
vi组件是圆点分隔组件
:::

### 基本使用

```vue

<template>
  <view>
    <myView width="750" height="100" background="yellow"></myView>
    <vi></vi>
    <myView width="750" height="100" background="yellow"></myView>

    <vi dot color="red" marginTop="32"></vi>
    <vi dot size="50" color="red" marginTop="32"></vi>
    <vi line color="red"></vi>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/vi1.png" :width="640" :height="437" style="margin-top: 20px"/>

### Props属性

| 参数           | 说明         | 类型            | 默认值       |   
|:-------------|:-----------|:--------------|:----------|
| size         | 大小/宽高      | Number\String | '32'      |
| height       | line的时候的高度 | Number\String | '2'       |
| line         | 是否线段       | Boolean       | false     |
| dot          | 是否为点       | Boolean       | false     |
| noBottom     | 底部是否有距离    | Boolean       | false     |
| color        | 颜色         | String        | '#f5f6f7' |
| marginTop    | 默认距离顶部     | Number\String | ''        |
| marginRight  | 右边距        | Number\String | ''        |
| marginBottom | 底边距        | Number\String | ''        |
| marginLeft   | 默认距离左侧     | Number\String | ''        |

