## myContent内容

::: tip 提示
myContent是内容显示组件,可以显示一些文字列表
:::

### 基本使用

```vue

<template>
  <view>
    <myCard display="flex" col>
      <myContent title="标题1" content="内容"></myContent>
      <myContent title="标题2" :list="[{title:'标题1'},{title:'标题2'}]"></myContent>
      <myContent title="标题3" :list="[{title:'标题1',unit:'个'},{title:'标题2'}]" showEdit showDel @edit="edit"
                 @del="del"></myContent>
    </myCard>
  </view>
</template>
<script>
  export default {
    methods: {
      edit(item, index) {
        console.log('编辑', item, index)
      },
      del(item, index) {
        console.log('删除', item, index);
      },
    }
  }
</script>

```

<imgPreview src="/develop/uniapp/myContent1.png" :width="632" :height="476" style="margin-top: 20px"/>

### Props属性

| 参数        | 说明       | 类型            | 默认值   |   
|:----------|:---------|:--------------|:------|
| title     | 标题       | String        | ''    |
| titleSize | 标题大小     | Number\String | '32'  |
| showTitle | 是否显示标题   | Boolean       | true  |
| content   | 内容       | String        | ''    |
| marginTop | 距离顶部     | Number\String | '32'  |
| showDot   | 显示文字前面的点 | Boolean       | true  |
| showDel   | 显示删除按钮   | Boolean       | false |
| showEdit  | 显示编辑按钮   | Boolean       | false |
| list      | 列表       | Array         | []    |

### Event事件

| 事件名  | 说明      | 回调参数 | 
|:-----|:--------|:-----|
| edit | 点击编辑时触发 | 无    |
| del  | 点击删除时触发 | 无    |
