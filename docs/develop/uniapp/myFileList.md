## myFileList文件列表

::: tip 提示
myFileList组件是xi
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myFileList value="文件1地址,文件2地址" marginTop="0"></myFileList>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myFileList1.png" :width="638" :height="283" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明              | 类型            | 默认值     |   
|:---------------|:----------------|:--------------|:--------|
| value          | 文件地址, 多个地址用逗号隔开 | String        | ''      |
| title          | 标题              | String        | ''      |
| titleSize      | 标题大小            | String        | '32'    |
| separator      | 分隔符             | String        | ','     |
| width          | 宽度              | Number\String | 196     |
| height         | 高度              | Number\String | 196     |
| marginTop      | 距离顶部            | Number\String | '32'    |
| showLook       | 是否显示查看按钮        | Boolean       | true    |
| showBorder     | 是否显示下边框线        | Boolean       | false   |
