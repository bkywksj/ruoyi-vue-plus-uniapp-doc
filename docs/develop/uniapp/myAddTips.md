## myAddTips收藏提示


::: tip 提示
myAddTips组件是提示用户收藏小程序的组件,默认10秒后自动消失,如果点击了.会设置记录到缓存,下次不会弹出,这个组件放在myNav插槽中
:::

### 基本使用

```vue

<template>
  <view>
    <myNav title="导航栏">
      <myAddTips></myAddTips>
    </myNav>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myAddTips1.png" :width="640" :height="230" style="margin-top: 20px"/>
