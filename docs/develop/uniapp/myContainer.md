## myContainer容器

::: tip 提示
myContainer组件一般作为容器,存放标题和内容, 可以展开和折叠 , 可以设置右侧文本方便执行更多逻辑, 可以作为购物车店铺下单选择器等
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer title="这是标题">
      <myText text="这是内容..." size="24"></myText>
    </myContainer>
    <myContainer title="这是标题" :open="true">
      <myText text="这是内容..." size="24"></myText>
    </myContainer>
    <myContainer title="这是标题" icon="/static/me/guanyu.png">
      <myText text="这是内容..." size="24"></myText>
    </myContainer>
    <myContainer title="这是标题" icon='-' showCheckbox v-model="isSelect">
      <myText text="这是内容..." size="24"></myText>
    </myContainer>
    <myContainer title="这是标题" showRight rightText="点击我" @tapRight="tapRight" marginBottom="32">
      <myText text="这是内容..." size="24"></myText>
    </myContainer>
  </view>
</template>

<script>
  export default {
    data() {
      return {
        isSelect: false
      }
    },
    methods: {
      tapRight() {
        this.$log('点击了右侧按钮')
      }
    }
  }
</script>

```

<imgPreview src="/develop/uniapp/myContainer1.png" :width="630" :height="765" style="margin-top: 20px"/>

### Props属性

| 参数               | 说明                                          | 类型            | 默认值                                    |   
|:-----------------|:--------------------------------------------|:--------------|:---------------------------------------|
| value            | 是否选中,当showCheckbox为真时,组件使用v-model可以绑定容器选中状态 | Boolean       | false                                  |
| icon             | 标题左侧的图标                                     | String        | ''                                     |
| title            | 容器标题                                        | String        | ''                                     |
| titleSize        | 容器标题字体大小                                    | Number\String | '36'                                   |
| titleColor       | 容器标题字体颜色                                    | String        | '#333333'                              |
| iconBorderRadius | 标题左侧的图标圆角                                   | String        | ''                                     |
| barColor         | 容器标题左侧竖线颜色                                  | Number\String | ''                                     |
| showCheckbox     | 容器标题左侧是否显示选择框                               | Boolean       | false                                  |
| open             | 容器标题右侧是否显示展开/收起                             | Boolean       | false                                  |
| position         | 容器相对位置属性                                    | String        | ''                                     |
| zIndex           | 容器层级                                        | Number\String | ''                                     |
| top              | 顶部                                          | Number\String | ''                                     |
| right            | 右侧                                          | Number\String | ''                                     |
| bottom           | 下边                                          | Number\String | ''                                     |
| left             | 左边                                          | Number\String | ''                                     |
| width            | 默认宽度686                                     | Number\String | '686'                                  |
| borderRadius     | 默认圆角16                                      | Number\String | '16'                                   |
| padding          | 默认内边距32                                     | Number\String | '32'                                   |
| marginTop        | 默认距离顶部32                                    | Number\String | '32'                                   |
| marginRight      | 距离右侧                                        | String        | ''                                     |
| marginBottom     | 距离底部                                        | String        | ''                                     |
| marginLeft       | 默认距离左侧32                                    | String        | '32'                                   |
| background       | 背景色,默认白色                                    | String        | 'white'                                |
| rightText        | 容器标题右侧文本                                    | String        | ''                                     |
| prefix           | 容器标题右侧文本前缀图标                                | String        | ''                                     |
| suffix           | 容器标题右侧文本后缀图标,默认小三角                          | String        | '/static/component/myContainer/sj.png' |
| showRight        | 是否显示标题右侧                                    | Boolean       | false                                  |
| showConfirm      | 是否显示确认按钮,为true则点击右侧的时候,会弹窗询问                | Boolean       | false                                  |

### Event事件

| 事件名      | 说明                | 回调参数   | 
|:---------|:------------------|:-------|
| tapTitle | 点击标题时触发           | 无      |
| tapRight | 点击标题右侧时触发         | 无      |
| confirm  | 点击容器时触发           | 无      |
| change   | 选中或取消选中标题左侧选择框时触发 | e,是否选中 |
