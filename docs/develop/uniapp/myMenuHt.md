## myMenuHt菜单管理

::: tip 提示
myMenuHt组件是商家后台管理菜单和商品的组件
:::

### 基本使用

```vue

<template>
  <view>
    <myMenuHt :tabbar="tabbar"></myMenuHt>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        tabbar: [{
          name: '日常用品',
          goodsList: [{
            goodsName: '商品1',
            mainImg: 'photo',
            price: '10',
            isCheck: '1',
            isDisplay: '1'
          }, {
            goodsName: '商品2',
            mainImg: 'photo',
            price: '10',
            isCheck: '2',
            isDisplay: '1'
          }]
        }, {
          name: '电脑器件',
          goodsList: [{
            goodsName: '商品3',
            mainImg: 'photo',
            price: '10',
            isCheck: '1',
            isDisplay: '0'
          }, {
            goodsName: '商品4',
            mainImg: 'photo',
            price: '10',
            isCheck: '0',
            isDisplay: '1'
          }]
        }],
      }
    },
  }
</script>
```

<imgPreview src="/develop/uniapp/myMenuHt1.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数     | 说明      | 类型    | 默认值 |   
|:-------|:--------|:------|:----|
| tabbar | 分类和商品信息 | Array | []  |

### tabbar每一项的属性

| 参数        | 说明                                                     | 类型     | 默认值 |   
|:----------|:-------------------------------------------------------|:-------|:----|
| name      | 分类名称                                                   | String | ''  |
| goodsList | 商品信息 goodsName mainImg price isCheck审核状态 isDisplay是否显示 | Object | {}  |

### Event事件

| 事件名 | 说明      | 回调参数 | 
|:----|:--------|:-----|
| ok  | 上架或下架成功 | 无    |
