## myOrder订单

::: tip 提示
myOrderd组件是订单组件,此组件比较复杂,且需要配合后端接口方能进行较好的展示
:::

### 基本使用

```vue

<template>
  <view>
    <myOrder :item="{list:[{
                shopName:'商店名称',
                orderMoney:'111',
                list:[{goodsName: '商品4',
              mainImg: 'photo',
              price: '10',
              buyNum: '10',
              gad:'规格'}]
            }]}"></myOrder>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myOrder1.png" :width="635" :height="320" style="margin-top: 20px"/>

### Props属性

| 参数   | 说明   | 类型     | 默认值 |   
|:-----|:-----|:-------|:----|
| item | 订单数据 | Object | {}  |
