## chatFooter底部聊天框

::: tip 提示
chatFooter组件是底部聊天框,如果是放h5中的tab界面,会不显示,因为距离底部0, 需要修改组件里面的样式bottom :0->100rpx
:::

### 基本使用

```vue

<template>
  <view>
    <chatFooter></chatFooter>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/chatFooter1.png" :width="620" :height="166" style="margin-top: 20px"/>

### Props属性

| 参数    | 说明        | 类型      | 默认值   |   
|:------|:----------|:--------|:------|
| value | 控制背板弹出与隐藏 | Boolean | false |
| type  | 类型        | String  | ''    |
| uid   | 用户id      | String  | ''    |
| toUid | 对方用户id    | String  | ''    |

### Event事件

| 事件名                | 说明     | 回调参数 | 
|:-------------------|:-------|:-----|
| scrollToBottom     | 滑到底部   | 无    |
| startSend          | 开始发送   | 无    |
| updateSendProgress | 更新发送进度 | 无    |
| sendComplete       | 发送完成   | 无    |
