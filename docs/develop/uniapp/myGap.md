## myGap间隔

::: tip 提示
myGap组件是间隔组件
:::

### 基本使用

```vue

<template>
  <view>
    <myView width="750" height="100" background="yellow"></myView>
    <myGap></myGap>
    <myView width="750" height="100" background="yellow"></myView>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myGap1.png" :width="641" :height="274" style="margin-top: 20px"/>

### Props属性

| 参数     | 说明   | 类型     | 默认值       |   
|:-------|:-----|:-------|:----------|
| width  | 默认宽度 | Number | 750       |
| height | 默认高度 | Number | 18        |
| color  | 颜色   | String | '#fafafa' |
