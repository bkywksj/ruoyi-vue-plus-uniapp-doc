## myCart购物车

::: tip 提示
myCart组件购物车组件,购物车组件数据存储在vuex中,想使用此组件需要实现组件内的相关方法
需要在vuex中实现 $cart() $setCartPrice() $setCart() 具体查看组件源码
:::

### 基本使用

```vue

<template>
  <view>
    <myCart v-model="open"></myCart>
  </view>
</template>

```

### Props属性

| 参数    | 说明   | 类型      | 默认值   |   
|:------|:-----|:--------|:------|
| value | 双向绑定 | Boolean | false |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
