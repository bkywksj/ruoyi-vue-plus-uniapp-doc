## myWaterfall瀑布流

::: tip 提示
myWaterfall组件是瀑布流组件
:::

### 基本使用

```vue

<template>
  <view>
    <!-- 左右长短一致 -->
    <myWaterfall :flowList="flowList">
    </myWaterfall>
    <!-- 左右长短不一 -->
    <myWaterfall isPb :flowList="flowList">
    </myWaterfall>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        flowList: [{
          mainImg: '/static/me/bg.png',
          goodsName: '名称1',
          price: '1',
          monthSale: 12
        },
          {
            mainImg: '/static/me/bg.png',
            goodsName: '名称1',
            price: '1',
            monthSale: 12
          },
          {
            mainImg: '/static/me/bg.png',
            goodsName: '名称1',
            price: '1',
            monthSale: 12
          }
        ],
      }
    },
  }
</script>
```

<imgPreview src="/develop/uniapp/myWaterfall1.png" :width="640" :height="1635" style="margin-top: 20px"/>

### Props属性

| 参数       | 说明    | 类型      | 默认值   |   
|:---------|:------|:--------|:------|
| isPb     | 是否瀑布流 | Boolean | false |
| flowList | 数据列表  | Array   | []    |

