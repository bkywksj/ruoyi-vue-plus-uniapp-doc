## myNav导航

::: tip 提示
myNav组件是导航组件，可以设置导航栏的样式，如背景颜色、字体颜色、字体大小等。
:::

### 基本使用

```vue

<template>
  <view>
    <myNav title="导航标题"></myNav>

    <myNav title="导航标题" isBack></myNav>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myNav1.png" :width="634" :height="97" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myNav2.png" :width="640" :height="87" style="margin-top: 20px"/>

### Props属性

| 参数            | 说明                                                  | 类型            | 默认值                                                                     |   
|:--------------|:----------------------------------------------------|:--------------|:------------------------------------------------------------------------|
| customBack    | 自定义返回逻辑方法，如传入，点击返回按钮执行函数，否则正常返回上一页，注意模板中不需要写方法参数的括号 | Function      | -                                                                       |
| zIndex        | 层级                                                  | Number\String | '980'                                                                   |
| titleColor    | 标题颜色                                                | String        | 'white'                                                                 |
| isFixed       | 是否固定                                                | Boolean       | true                                                                    |
| isBack        | 是否显示返回                                              | Boolean       | false                                                                   |
| borderBottom  | 导航栏底部是否显示下边框，如定义了较深的背景颜色，可取消此值                      | Boolean       | false                                                                   |
| background    | 右侧                                                  | Object        | {'background-image': 'linear-gradient(45deg,#3377ff 0%, #3377ff 100%)'} |
| title         | 导航文字                                                | String        | ''                                                                      |
| backIconName  | 返回图标                                                | String        | 'nav-back'                                                              |
| backText      | 返回提示                                                | String        | '返回'                                                                    |
| backTextColor | 返回提示文字颜色                                            | String        | 'white'                                                                 |
| backTextSize  | 返回提示文字大小                                            | Number\String | '30'                                                                    |
