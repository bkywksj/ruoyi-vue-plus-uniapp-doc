## mySelect选择

::: tip 提示
mySelect组件是是否选择组件
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <mySelect text="是否选择" v-model="isSelect"></mySelect>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/mySelect1.png" :width="619" :height="205" style="margin-top: 20px"/>

### Props属性

| 参数              | 说明                | 类型            | 默认值   |   
|:----------------|:------------------|:--------------|:------|
| value           | 双向绑定 是否选择         | Boolean       | false |
| show            | 是否显示              | Boolean       | true  |
| canReverse      | 是否可以反选 默认可以选中 非选中 | Boolean       | true  |
| marginTop       | 默认距离顶部            | Number\String | ''    |
| marginRight     | 右边距               | Number\String | ''    |
| marginBottom    | 底边距               | Number\String | ''    |
| marginLeft      | 默认距离左侧            | Number\String | ''    |
| iconMarginRight | 图标距离右侧            | Number\String | '8'   |
| text            | 提示文本              | String        | ''    |

### Event事件

| 事件名    | 说明    | 回调参数     | 
|:-------|:------|:---------|
| change | 点击时触发 | isSelect |
