## myBottomBtn底部按钮

::: tip 提示
myBottomBtn组件是底部按钮组件，主要用来做底部按钮。
:::

### 基本使用

```vue

<template>
  <view>
    <myBottomBtn title="这是按钮1"></myBottomBtn>
  </view>
</template>

<template>
  <view>
    <myBottomBtn titleTwo="这是按钮2" @confirm="confirm"></myBottomBtn>
  </view>
</template>

<template>
  <view>
    <myBottomBtn titleTwo="这是按钮3" widthTwo="200" @confirm="confirm" @confirmTwo="confirmTwo" showConfirm>
    </myBottomBtn>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myBottomBtn1.png" :width="626" :height="219" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myBottomBtn2.png" :width="627" :height="222" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myBottomBtn3.png" :width="628" :height="202" style="margin-top: 20px"/>

### Props属性

| 参数           | 说明          | 类型      | 默认值       |   
|:-------------|:------------|:--------|:----------|
| title        | 按钮标题        | String  | '按钮标题'    |
| type         | 按钮类型        | String  | 'primary' |
| shape        | 按钮形状        | String  | ''        |
| show         | 是否显示        | Boolean | true      |
| showConfirm  | 是否显示确认提示    | Boolean | false     |
| showOne      | 是否显示第一个按钮   | Boolean | true      |
| typeTwo      | 第二个按钮类型     | String  | 'primary' |
| titleTwo     | 第二个按钮标题     | String  | ''        |
| shapeTwo     | 第二个按钮形状     | String  | ''        |
| widthTwo     | 第二个按钮宽度     | String  | '328'     |
| inputText    | 第一个是否弹窗输入原因 | Boolean | false     |
| inputTextTwo | 第二个是否弹窗输入原因 | Boolean | false     |

### Event事件

| 事件名        | 说明         | 回调参数 | 
|:-----------|:-----------|:-----|
| confirm    | 点击时触发      | 无    |
| confirmTwo | 点击第二个按钮时触发 | 无    |
