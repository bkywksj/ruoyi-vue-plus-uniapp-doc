## myForm表单容器

::: tip 提示
myForm表单作为表单的容器,对h5兼容良好,但是对小程序兼容性不好,需要使用u-form,具体示例在temp目录下的form.vue中进行参考
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <myForm :model="form" :rules="rules">
        <formInput label="名称" v-model="form.name"></formInput>
      </myForm>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myForm1.png" :width="624" :height="762" style="margin-top: 20px"/>

### Props属性

| 参数           | 说明               | 类型      | 默认值  |   
|:-------------|:-----------------|:--------|:-----|
| model        | 当前form的需要验证字段的集合 | Object  | {}   |
| rules        | 规则               | Object  | {}   |
| confirmTitle | 确认标题             | String  | '提交' |
| showConfirm  | 是否确认             | Boolean | true |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 提交时触发 | 无    |
