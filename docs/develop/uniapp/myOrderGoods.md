## myOrderGoods订单商品

::: tip 提示
myOrderGoods组件是订单商品
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myOrderGoods :item="{goodsName: '商品4',
              mainImg: 'photo',
              price: '10',
              buyNum: '10',
              gad:'规格'}"></myOrderGoods>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myOrderGoods1.png" :width="620" :height="277" style="margin-top: 20px"/>

### Props属性

| 参数   | 说明   | 类型     | 默认值 |   
|:-----|:-----|:-------|:----|
| item | 商品详情 | Object | {}  |

### item属性

| 参数        | 说明   | 类型     | 默认值 |   
|:----------|:-----|:-------|:----|
| mainImg   | 商品图片 | String | ''  |
| goodsName | 商品名称 | String | ''  |
| price     | 商品价格 | String | ''  |
| buyNum    | 购买数量 | String | ''  |
| gad       | 商品规格 | String | ''  |
