## myBottomPop底部弹窗

::: tip 提示
myBottomPop组件是底部弹窗组件
:::

### 基本使用

```vue

<template>
  <view>
    <myBottomPop title="弹窗" tip="提示" v-model="showPop">
      <myView>
        这是内容
      </myView>
    </myBottomPop>
    <myBtn title="打开弹窗" @confirm="showPop=true"></myBtn>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myBottomPop1.png" :width="640" :height="1366" style="margin-top: 20px"/>

### Props属性

| 参数          | 说明                              | 类型            | 默认值       |   
|:------------|:--------------------------------|:--------------|:----------|
| value       | 弹窗打开双向绑定                        | Boolean       | false     |
| mode        | 模式 控制位置                         | String        | 'bottom'  |
| showConfirm | 是否显示确认按钮                        | Boolean       | true      |
| zIndex      | 层级                              | Number\String | '1000'    |
| width       | 默认宽度                            | String        | ''        |
| height      | 默认高度                            | Number\String | ''        |
| padding     | 默认内边距                           | Number\String | '32'      |
| title       | 标题                              | String        | ''        |
| titleSize   | 标题大小                            | Number\String | '36'      |
| titleColor  | 标题颜色                            | Number\String | ''        |
| tip         | 提示                              | String        | ''        |
| type        | warning error info success 按钮样式 | String        | 'primary' |
| shape       | 形状                              | String        | 'square'  |
| btnTitle    | 按钮标题                            | String        | '确定'      |

### Event事件

| 事件名     | 说明     | 回调参数 | 
|:--------|:-------|:-----|
| open    | 打开弹时触发 | 无    |
| confirm | 确认时触发  | 无    |
| close   | 关闭时触发  | 无    |
