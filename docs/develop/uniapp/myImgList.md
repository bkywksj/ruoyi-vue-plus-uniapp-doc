## myImgList图片列表

::: tip 提示
myImgList组件显示图片列表的组件,支持预览
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myImgList title="图片列表" value="/static/me/guanyu.png,/static/me/htgl.png">
      </myImgList>
    </myCard>
  </view>
</template>
```

<imgPreview src="/develop/uniapp/myImgList1.png" :width="624" :height="327" style="margin-top: 20px"/>

### Props属性

| 参数         | 说明              | 类型            | 默认值   |   
|:-----------|:----------------|:--------------|:------|
| value      | 图片地址列表,逗号隔开的字符串 | String        | ''    |
| stop       | 是否防止冒泡          | Boolean       | false |
| title      | 图片列表标题          | String        | ''    |
| titleSize  | 图片列表标题字体大小      | Number\String | '32'  |
| separator  | 图片分隔符           | String        | ','   |
| width      | 图片宽度            | Number\String | 192   |
| height     | 图片高度            | Number\String | 192   |
| marginTop  | 默认距离顶部          | Number\String | '32'  |
| showLook   | 是否显示预览标签        | Boolean       | false |
| showBorder | 是否显示底部边框        | Boolean       | false |

### Event事件
无
