## myLinkman联系人

::: tip 提示
myLinkman组件是底部联系人组件，可以收藏,分享,聊天,电话或者自定义功能
:::

### 基本使用

```vue

<template>
  <view>
    <myLinkman></myLinkman>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myLinkman1.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数         | 说明     | 类型            | 默认值                                       |   
|:-----------|:-------|:--------------|:------------------------------------------|
| type       | 收藏类型   | String        | ''                                        |
| projectId  | 项目id   | String        | ''                                        |
| showLeft   | 是否显示左侧 | Boolean       | true                                      |
| leftWidth  | 左侧宽度   | Number\String | '182'                                     |
| leftIcon   | 左侧图标   | String        | '/static/component/myLinkman/talk.png'    |
| leftTitle  | 左侧标题   | String        | '聊天'                                      |
| rightWidth | 右侧宽度   | Number\String | '182'                                     |
| rightIcon  | 右侧图标   | String        | ''/static/component/myLinkman/phone.png'' |
| rightTitle | 右侧文字   | Number\String | ''                                        |
| item       | 绑定详情   | Object        | {}                                        |

### Event事件

| 事件名      | 说明      | 回调参数 | 
|:---------|:--------|:-----|
| tapLeft  | 点击左侧时触发 | 无    |
| tapRight | 点击右侧时触发 | 无    |
