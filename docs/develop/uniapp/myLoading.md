## myLoading加载动画

::: tip 提示
myLoading是页面加载动画组件，一般用于页面加载动画。有多种加载样式，通过设置不同的type属性即可。
:::

### 基本使用

```vue

<template>
  <view>
    <myLoading v-if="true"></myLoading>
    <myLoading type="circle" v-if="true"></myLoading>
    <myLoading type="pulse" v-if="true"></myLoading>
    <myLoading type="bounce" v-if="true" title="页面正在加载..."></myLoading>
    <myLoading type="eyes" v-if="true" mask></myLoading>
    <myLoading type="triangle" v-if="true"></myLoading>
    <myLoading type="sun" v-if="true"></myLoading>
    <myLoading type="love" v-if="true"></myLoading>
    <myLoading type="sword" v-if="true"></myLoading>
    <myLoading type="atom" v-if="true"></myLoading>
    <myLoading type="gear" v-if="true"></myLoading>

  </view>
</template>

```

<p>以下效果图片从左到右</p>
<imgPreview src="/develop/uniapp/myLoading1.png" :width="614" :height="311" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading2.png" :width="605" :height="257" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading3.png" :width="604" :height="299" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading4.png" :width="630" :height="301" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading5.png" :width="577" :height="285" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading6.png" :width="532" :height="283" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading7.png" :width="544" :height="322" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading8.png" :width="576" :height="302" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading9.png" :width="441" :height="266" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading10.png" :width="571" :height="260" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myLoading11.png" :width="500" :height="254" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明                                                                | 类型            | 默认值      |   
|:---------------|:------------------------------------------------------------------|:--------------|:---------|
| type           | 加载动画类型 circle pulse bounce eyes triangle sun love sword atom gear | String        | 'atom'   |
| title          | 标题                                                                | String        | '加载中...' |
| position       | 位置                                                                | String        | 'fixed'  |
| zIndex         | 层级                                                                | Number        | 9999     |
| mask           | 是否遮罩层                                                             | Boolean       | false    |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
