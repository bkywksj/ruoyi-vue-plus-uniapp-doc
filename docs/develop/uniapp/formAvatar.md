## formAvatar头像

::: tip 提示
formAvatar组件是表单头像组件,用于上传头像
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formAvatar v-model="form.avatar"></formAvatar>
      </u-form>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myFormAvatar1.png" :width="631" :height="264" style="margin-top: 20px"/>

### Props属性

| 参数         | 说明     | 类型            | 默认值                     |   
|:-----------|:-------|:--------------|:------------------------|
| value      | 双向绑定的值 | String        | ''                      |
| col        | 是否竖直显示 | Boolean       | false                   |
| label      | 标签     | String        | '头像'                    |
| tip        | 提示     | String        | 'tip：请点击右侧头像，选择“用微信头像”' |
| labelStyle | 标签样式   | Object        | {}                      |
| size       | 头像大小   | Number\String | 100                     |
| marginTop  | 默认距离顶部 | Number\String | '32'                    |
| prop       | 校验属性   | String        | ''                      |
| required   | 是否必填   | Boolean       | false                   |

