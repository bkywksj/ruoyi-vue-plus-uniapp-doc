## mySearch搜索

::: tip 提示
mySearch是搜索框组件
:::

### 基本使用

```vue

<template>
  <view>
    <mySearch marginTop="50"></mySearch>
    <mySearch marginTop="50" background="red" bgColor="yellow"></mySearch>
    <mySearch shape="round" marginTop="50" @search="search" @custom="search" @click="click"></mySearch>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/mySearch1.png" :width="629" :height="369" style="margin-top: 20px"/>

### Props属性

| 参数              | 说明              | 类型            | 默认值           |   
|:----------------|:----------------|:--------------|:--------------|
| value           | 关键词             | String        | ''            |
| fixed           | 是否固定            | Boolean       | false         |
| shape           | 形状 square round | String        | 'square'      |
| placeholder     | 填充值             | String        | '请输入关键字'      |
| bgColor         | 搜索框背景色          | String        | '#f7f7f7'     |
| searchIconColor | 搜索图标颜色          | String        | ''            |
| disabled        | 是否禁用            | Boolean       | false         |
| clearabled      | 是否有清空按钮         | Boolean       | true          |
| animation       | 是否有动画           | Boolean       | false         |
| showAction      | 是否显示右侧控件        | Boolean       | true          |
| actionStyle     | 构造右侧按钮样式        | Object        | {}            |
| background      | 背景色             | String        | 'white'       |
| width           | 宽度              | Number\String | '750'         |
| padding         | 默认内边距           | Number\String | '12rpx 32rpx' |
| height          | 默认高度            | Number\String | '64'          |
| marginTop       | 默认距离顶部          | Number\String | '32'          |
| marginRight     | 右边距             | Number\String | ''            |
| marginBottom    | 底边距             | Number\String | ''            |
| marginLeft      | 默认距离左侧          | Number\String | '32'          |
| positon         | 相对位置            | String        | ''            |
| zIndex          | 背景层级            | Number\String | ''            |
| top             | 距离顶部            | Number\String | ''            |
| right           | 距离右侧            | Number\String | ''            |
| bottom          | 距离底部            | Number\String | ''            |
| left            | 距离左侧            | Number\String | ''            |

### Event事件

| 事件名     | 说明              | 回调参数 | 
|:--------|:----------------|:-----|
| search  | 搜索点击时触发         | e    |
| change  | 搜索值变化时触发        | e    |
| confirm | 禁用状态 且单击 才触发此方法 | 无    |
