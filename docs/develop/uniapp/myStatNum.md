## myStatNum统计数字

::: tip 提示
myStatNum组件是统计展示组件
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myView>
        <myStatNum :value="10" label="人数"></myStatNum>
        <myStatNum :value="10" label="人数"></myStatNum>
        <myStatNum :value="10" label="人数"></myStatNum>
        <myStatNum :value="10" label="人数"></myStatNum>
      </myView>
      <myView marginTop="32">
        <myStatNum :value="10" label="人数"></myStatNum>
        <myStatNum :value="10" label="人数"></myStatNum>
        <myStatNum :value="10" label="人数"></myStatNum>
        <myStatNum :value="10" label="人数"></myStatNum>
      </myView>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myStatNum1.png" :width="624" :height="305" style="margin-top: 20px"/>

### Props属性

| 参数    | 说明   | 类型            | 默认值 |   
|:------|:-----|:--------------|:----|
| value | 绑定的值 | Number\String | ''  |
| label | 标签   | String        | ''  |

