## myNavBg导航背景

::: tip 提示
myNavBg组件是导航背景图片.可以配合myNav组件,实现渐变切换的效果
:::

### 基本使用

```vue

<template>
  <view>
    <myNavBg></myNavBg>

    <myNavBg img="/static/me/bg.png" opacity="1">
    </myNavBg>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myNavBg1.png" :width="626" :height="853" style="margin-top: 20px"/>

### Props属性

| 参数      | 说明  | 类型            | 默认值                      |   
|:--------|:----|:--------------|:-------------------------|
| opacity | 透明度 | Number        | 1                        |
| height  | 高度  | Number\String | 500                      |
| deg     | 角度  | Number        | 180                      |
| color1  | 颜色1 | String        | 'rgba(254, 251, 216, 1)' |
| color2  | 颜色2 | String        | 'rgba(247, 247, 247, 1)' |
| img     | 图片  | String        | ''                       |
