## formNumberBox数字输入框

::: tip 提示
formNumberBox组件是数字输入框组件
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formNumberBox label="数量" v-model="form.num"></formNumberBox>
        <formNumberBox label="数量2" :step="10" v-model="form.num2"></formNumberBox>
        <formNumberBox label="数量3" :min="20" :max="30" tip="请输入数量" v-model="form.num3" @change="change">
        </formNumberBox>
      </u-form>
    </myContainer>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        form: {
          num1: 0,
          num2: 0,
          num3: 20
        }
      }
    },
    methods: {
      change(e) {
        console.log("发生变化", e)
      },
    }
  }
</script>
```

<imgPreview src="/develop/uniapp/formNumberBox1.png" :width="622" :height="428" style="margin-top: 20px"/>

### Props属性

| 参数              | 说明                     | 类型            | 默认值    |   
|:----------------|:-----------------------|:--------------|:-------|
| value           | 双向绑定的值                 | Number        | 0      |
| col             | 是否竖直方向                 | Boolean       | true   |
| width           | 表单宽度                   | Number\String | ''     |
| marginTop       | 默认距离顶部                 | Number\String | '0'    |
| position        | 相对位置                   | String        | ''     |
| prop            | 校验属性                   | String        | ''     |
| label           | 标签                     | String        | ''     |
| labelWidth      | 标签宽度                   | Number\String | ''     |
| labelStyle      | 标签样式                   | Object        | {}     |
| labelAlign      | 标签对齐方式                 | String        | ''     |
| leftIcon        | 左侧图标                   | String        | ''     |
| leftIconStyle   | 左侧图标样式                 | Object        | {}     |
| rightIcon       | 右侧图标                   | String        | ''     |
| rightIconStyle  | 右侧图标样式                 | Object        | {}     |
| borderBottom    | 是否显示下划线                | Boolean       | true   |
| required        | 是否必填                   | Boolean       | false  |
| tip             | 提示                     | String        | ''     |
| step            | 步进值                    | Number        | 1      |
| min             | 最小值                    | Number        | 0      |
| max             | 最大值                    | Number        | 999999 |
| disabledInput   | 是否禁用手动输入               | Boolean       | false  |
| longPress       | 是否开启长按连续递增或递减          | Boolean       | true   |
| positiveInteger | 是否只能输入正整数              | Boolean       | true   |
| index           | 事件回调时用以区分当前发生变化的是哪个输入框 | String        | '0'    |
| inputWidth      | 输入框宽度                  | Number\String | '120'  |
| inputHeight     | 输入框高度                  | Number\String | '50'   |

### Event事件

| 事件名    | 说明       | 回调参数 | 
|:-------|:---------|:-----|
| change | 输入值改变时触发 | e    |
