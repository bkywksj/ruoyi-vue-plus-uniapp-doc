## myText文本

::: tip 提示
myText组件是uniapp中比较基础的组件.可以方便地设置文本,前缀图标,后缀图标等
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myText text="这是文本"></myText>
      <myText text="这是文本2" prefix="chat" marginTop="32"></myText>
      <myText text="这是文本3" suffix="lock" iconColor="red" iconSize="60"></myText>
      <myText text="100" money @confirm="confirm"></myText>
    </myCard>
  </view>
</template>
<script>
  export default {
    methods: {
      confirm() {
        console.log('点击了确认按钮')
      },
    }
  }
</script>
```

<imgPreview src="/develop/uniapp/myText1.png" :width="636" :height="270" style="margin-top: 20px"/>

### Props属性

| 参数               | 说明                                                                      | 类型            | 默认值      |   
|:-----------------|:------------------------------------------------------------------------|:--------------|:---------|
| text             | 文本内容                                                                    | String        | ''       |
| show             | 是否显示                                                                    | Boolean       | false    |
| money            | 是否显示金额单位                                                                | Boolean       | false    |
| col              | 是否竖直方向                                                                  | Boolean       | false    |
| type             | 类型 1代表主要颜色 2代表次要颜色 3代表备注颜色 primary蓝色 success绿色 warning橙色 error红色 info灰色 | String        | ''       |
| position         | 相对位置                                                                    | String        | ''       |
| zIndex           | 层级                                                                      | Number\String | ''       |
| top              | 顶部                                                                      | Number\String | ''       |
| right            | 右侧                                                                      | Number\String | ''       |
| bottom           | 下边                                                                      | Number\String | ''       |
| left             | 左边                                                                      | Number\String | ''       |
| width            | 默认宽度                                                                    | Number\String | ''       |
| height           | 默认高度                                                                    | Number\String | ''       |
| maxWidth         | 最大宽度                                                                    | Number\String | ''       |
| minWidth         | 最小宽度                                                                    | Number\String | ''       |
| href             | 跳转链接 包括路径和网址                                                            | String        | ''       |
| prefix           | 前置图标 可为图片可为icon                                                         | String        | ''       |
| suffix           | 后置图标  /static/cloudCompany/sj.png  小三角                                  | String        | ''       |
| iconSize         | 图标大小                                                                    | String        | '32'     |
| iconColor        | 图标颜色 默认跟随文本 设置了才采用自定义                                                   | String        | ''       |
| iconMarginTop    | 图标距离顶部                                                                  | Number\String | '2'      |
| iconMarginRight  | 图标距离右侧                                                                  | Number\String | '4'      |
| iconMarginBottom | 图标距离底部                                                                  | Number\String | ''       |
| iconMarginLeft   | 图标距离左侧                                                                  | Number\String | '4'      |
| iconBorderRadius | 图标圆角                                                                    | Number\String | ''       |
| selectable       | 是否可选                                                                    | Boolean       | fasle    |
| block            | 是否块状 块状就跟view一样 占一行                                                     | Boolean       | fasle    |
| lines            | 超过多少行省略                                                                 | Number\String | ''       |
| color            | 文字颜色                                                                    | String        | ''       |
| size             | 文字大小                                                                    | Number\String | '28'     |
| fontStyle        | 字体样式 normal italic oblique inherit                                      | String        | ''       |
| decoration       | 文字装饰                                                                    | String        | ''       |
| whiteSpace       | 空白符 normal pre nowrap pre-wrap pre-line inherit                         | String        | ''       |
| wordBreak        | 单词换行 break-all keep-all                                                 | String        | ''       |
| textIndent       | 是否空俩格                                                                   | Boolean       | false    |
| marginTop        | 默认距离顶部                                                                  | Number\String | '2'      |
| marginRight      | 右边距                                                                     | Number\String | ''       |
| marginBottom     | 底边距                                                                     | Number\String | ''       |
| marginLeft       | 默认距离左侧                                                                  | Number\String | ''       |
| padding          | 文本内边距                                                                   | Number\String | ''       |
| borderRadius     | 文本容器圆角                                                                  | Number\String | ''       |
| background       | 背景色                                                                     | Number\String | 'white'  |
| bgWidth          | 外层宽度                                                                    | Number\String | ''       |
| bgHeight         | 外层高度                                                                    | Number\String | ''       |
| flex             | 布局                                                                      | Number\String | ''       |
| bgPadding        | 外层内边距                                                                   | Number\String | ''       |
| bgBackground     | 外层背景颜色                                                                  | String        | ''       |
| bgBorderRadius   | 外层圆角                                                                    | String        | ''       |
| bgJustifyContent | 外层对齐                                                                    | String        | 'center' |
| bgAlignItems     | 外层对齐                                                                    | String        | ''       |
| bgTextAlign      | 外层对齐                                                                    | String        | ''       |
| justifyContent   | 对齐                                                                      | Number\String | ''       |
| alignItems       | 对齐                                                                      | String        | ''       |
| textAlign        | 对齐                                                                      | String        | ''       |
| call             | 是否拨打                                                                    | Boolean       | false    |
| showConfirm      | 是否显示确认                                                                  | Boolean       | false    |
| tip              | 确认提示语                                                                   | String        | ''       |
| cancelText       | 取消提示语                                                                   | String        | '取消'     |
| cancelColor      | 取消颜色                                                                    | String        | ''       |
| confirmText      | 确定提示语                                                                   | String        | '确定'     |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
| cancel  | 取消时触发 | 无    |
