## myOpenType开放类型

::: tip 提示
myOpenType组件是开放类型组件,小程序使用,用于分享,获取手机号,客服等 详见https:
//uniapp.dcloud.net.cn/component/button.html#button
注意:本组件必须上级是相对定位的容器, 会铺满父容器,大小由父容器决定
:::

### 基本使用

```vue

<template>
  <view>
    <myView position="relative" width="200" height="100" background="red">
      <myOpenType openType="share"></myOpenType>
    </myView>

    <myView position="relative" width="200" height="100" background="yellow">
      <myOpenType openType="getPhoneNumber" @getTel="getTel"></myOpenType>
    </myView>

    <myMenuItem icon="/static/me/kf.png" title="联系开发者" position="relative">
      <myOpenType openType="contact"></myOpenType>
    </myMenuItem>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myOpenType1.png" :width="633" :height="359" style="margin-top: 20px"/>

### Props属性

| 参数       | 说明   | 类型     | 默认值     |   
|:---------|:-----|:-------|:--------|
| openType | 开放类型 | String | 'share' |

### Event事件

| 事件名    | 说明      | 回调参数 | 
|:-------|:--------|:-----|
| getTel | 获取手机时触发 | e    |
