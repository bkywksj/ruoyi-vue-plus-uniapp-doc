## myPoster海报

::: tip 提示
myPoster组件是海报组件,可以通过传json对象的形式构建海报,更详细介绍查看myPoster里面的readme文档.组件名称修改了,原理一样
:::

### 基本使用

```vue

<template>
  <view>
    <myPoster isRenderImage :board="poster" @success="success"></myPoster>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        poster: {
          width: '686rpx',
          height: '130rpx',
          views: [{
            type: 'view',
            css: {
              left: '0rpx',
              top: '0rpx',
              background: '#07c160',
              width: '120rpx',
              height: '120rpx'
            }
          },
            {
              type: 'view',
              css: {
                left: '180rpx',
                top: '18rpx',
                background: '#1989fa',
                width: '80rpx',
                height: '80rpx',
                transform: 'transform: rotate(50deg)'
              }
            }
          ]
        },
      }
    },
    methods: {
      success(e) {
        console.log('e', e);
      },
    }
  }
</script>
```

<imgPreview src="/develop/uniapp/myPoster1.png" :width="500" :height="254" style="margin-top: 20px"/>

### Props属性

| 参数            | 说明     | 类型      | 默认值   |   
|:--------------|:-------|:--------|:------|
| board         | 海报对象   | Object  | {}    |
| fileType      | 文件类型   | String  | 'png' |
| quality       | 质量     | Number  | 1     |
| isRenderImage | 是否渲染图片 | Boolean | false |

