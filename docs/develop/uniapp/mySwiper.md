## mySwiper轮播

::: tip 提示
图片轮播组件,和myImgSwiper相似,可以根据tupe不同自动加载后台配置的广告轮播
:::

### 基本使用

```vue

<template>
  <view>
    <myImgSwiper :swiperList="swiperList"></myImgSwiper>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        swiperList: [{
          image: 'https://cdn.uviewui.com/uview/swiper/1.jpg',
          title: '昨夜星辰昨夜风，画楼西畔桂堂东'
        }, {
          image: 'https://cdn.uviewui.com/uview/swiper/2.jpg',
          title: '身无彩凤双飞翼，心有灵犀一点通'
        }],
      }
    },
  }
</script>
```

<imgPreview src="/develop/uniapp/mySwiper1.png" :width="627" :height="164" style="margin-top: 20px"/>

### Props属性

| 参数                     | 说明                                         | 类型            | 默认值             |   
|:-----------------------|:-------------------------------------------|:--------------|:----------------|
| type                   | 类型,如果提供了且swiperList为空,则随机查询三个商品图片,这里自己扩展调整 | String        | ''              |
| mode                   | 指示器模式,rect / dot / number / none           | String        | ''              |
| showBg                 | 是否显示椭圆背景                                   | Boolean       | fasle           |
| showTitle              | 是否显示标题                                     | Boolean       | fasle           |
| effect3d               | 是否开启3D效果                                   | Boolean       | fasle           |
| effect3dPreviousMargin | effect3d = true模式的情况下，激活项与前后项之间的距离，单位rpx   | Number\String | '50'            |
| swiperHeight           | 轮播图高度                                      | String        | '270'           |
| bgColor                | 背景颜色                                       | String        | 'rgba(0,0,0,0)' |
| swiperList             | 轮播图列表                                      | Array         | ''              |
| width                  | 宽度                                         | Number\String | '750'           |
| marginTop              | 距离顶部                                       | Number\String | ''              |
| borderRadius           | 圆角                                         | Number\String | '8'             |
| padding                | 内边距                                        | Number\String | '0 32rpx'       |
| position               | 内边距                                        | String        | 'relative'      |

### Event事件

| 事件名         | 说明       | 回调参数 | 
|:------------|:---------|:-----|
| change      | 轮播图切换时触发 | 无    |
| clickSwiper | 轮播图点击时触发 | 无    |
