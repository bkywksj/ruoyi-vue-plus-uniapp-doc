## myWave波纹动态边框

::: tip 提示
myWave组件是波纹组件
:::

### 基本使用

```vue

<template>
  <view>
    <myWave></myWave>

    <myWave size="200">
      <myImg position="relative" src="/static/me/avatar.png" size="144" borderRadius="50%"></myImg>
    </myWave>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myWave1.png" :width="329" :height="380" style="margin-top: 20px"/>

### Props属性

| 参数           | 说明     | 类型            | 默认值     |   
|:-------------|:-------|:--------------|:--------|
| show         | 是否显示   | Boolean       | true    |
| position     | 相对位置   | String        | ''      |
| zIndex       | 层级     | Number\String | ''      |
| marginTop    | 默认距离顶部 | Number\String | ''      |
| marginRight  | 右边距    | Number\String | ''      |
| marginBottom | 底边距    | Number\String | ''      |
| marginLeft   | 默认距离左侧 | Number\String | ''      |
| top          | 顶部     | Number\String | ''      |
| right        | 右侧     | Number\String | ''      |
| bottom       | 下边     | Number\String | ''      |
| left         | 左边     | Number\String | ''      |
| num          | 波纹数量   | Number        | 3       |
| size         | 波纹大小   | Number\String | '144'   |
| delay        | 延迟     | Number        | 0.5     |
| borderRadius | 圆角     | String        | '50%'   |
| border       | 边框     | String        | ''      |
| background   | 背景色    | Number\String | 'white' |
