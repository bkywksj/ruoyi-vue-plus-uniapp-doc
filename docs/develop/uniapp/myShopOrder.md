## myShopOrder商城订单

::: tip 提示
myShopOrder组件是用户订单组件,显示购买用户的信息,订单列表,完善可以发起会话等
:::

### 基本使用

```vue

<template>
  <view>
    <myShopOrder :item="{
            orderNo:'21111',
            user:{
                uid:'',
                nickname:'昵称',
                avatar:'chat'
            },
            address:{
                name:'广东',
                tel:'111',
                area:'222',
                address:'333'
            },
            list:[{
                shopName:'商店名称',
                orderMoney:'111',
                list:[{goodsName: '商品4',
                isPay:'1',
                createTime:'2020-10-10',
              mainImg: 'photo',
              buyNum: '10',
              orderMoney: '10',
              gad:'规格'}]
            }]}">
    </myShopOrder>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myShopOrder1.png" :width="629" :height="459" style="margin-top: 20px"/>

