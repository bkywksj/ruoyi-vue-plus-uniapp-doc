## myTabs标签

::: tip 提示
myTabs组件是tabs标签组件,用于展示分类
:::

### 基本使用

```vue

<template>
  <view>
    <myTabs :tabs="tabs" @change="tabChange"/>
    <myTabs bgColor="red" inactiveColor="white" activeColor="yellow" :tabs="tabs" @change="tabChange"/>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        tabs: [{
          name: '手机',
          count: 10
        }, {
          name: '电视',
          count: 2
        }, {
          name: '家电'
        }, {
          name: '笔记本'
        }],
      }
    },
  }
</script>
```

<imgPreview src="/develop/uniapp/myTabs1.png" :width="632" :height="159" style="margin-top: 20px"/>

### Props属性

| 参数              | 说明                  | 类型            | 默认值       |   
|:----------------|:--------------------|:--------------|:----------|
| tabs            | tabs数组 name count属性 | Array         | []        |
| current         | 当前激活                | Number\String | 0         |
| fixed           | 是否固定                | Boolean       | false     |
| zIndex          | 层级                  | String        | '100'     |
| marginTop       | 距离顶部                | Number\String | ''        |
| bgColor         | 背景色                 | String        | 'white'   |
| width           | 宽度                  | Number\String | '750'     |
| isScroll        | 是否可以滚动              | Boolean       | false     |
| bold            | 是否加粗                | Boolean       | false     |
| fontSize        | 字体大小                | String        | '32'      |
| inactiveColor   | 未激活颜色               | String        | '#333333' |
| activeColor     | 激活颜色                | String        | '#3371FF' |
| activeItemStyle | 激活样式                | Object        | {}        |
| gutter          | 标签间距                | String        | '30'      |
| offset          | count属性偏移           | Array         | [15, 16]  |
| showBar         | 是否显示底部条             | Boolean       | true      |
| barHeight       | 底部条高度               | Number\String | '8'       |
| barWidth        | 底部条宽度               | Number\String | '32'      |
| barStyle        | 底部条样式               | Object        | {}        |
| duration        | 切换动画时长              | Number\String | '0.5'     |
| height          | 高度                  | Number\String | '92'      |

### Event事件

| 事件名    | 说明       | 回调参数 | 
|:-------|:---------|:-----|
| change | 切换tab时触发 | e    |
