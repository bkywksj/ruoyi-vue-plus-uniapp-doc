## myPage分页

::: tip 提示
myPage是一个下滑翻页组件,可以方便地实现下滑翻页效果,也可以设置搜索按钮等,无数据时也会显示无数据提示,在temp目录有完善的使用示例,myPage作为一个内容容器,内部是vfor的内容,下滑后会加载新数据合并到list中实现下拉加载更多
:::

### 基本使用

```vue

<template>
  <!-- 模板文件 -->
  <view class="content">

    <!-- 导航栏 -->
    <!-- #ifdef MP-WEIXIN -->
    <view class="myNav">
      <myNav title="模板文件" isBack/>
    </view>
    <!-- #endif -->

    <myTabs :current="current" :tabs="tabs" @change="tabChange" fixed/>

    <myPage showSearch :total="total" showTotal searchFixed :isEnd="isEnd" @search="search">
      <myCard :item="item" v-for="(item,index) in list" :key="index">
        ---
      </myCard>
    </myPage>

  </view>
</template>

<script>
  export default {
    data() {
      return {
        tabs: [{
          name: '栏目A',
        }, {
          name: '栏目B',
        }, {
          name: '栏目C',
        }],
        current: 0,

        list: undefined,
        total: undefined,
        isEnd: undefined,
        queryParams: {
          searchValue: '',
          pageNum: 1,
          pageSize: 10,
          orderByColumn: 'updateTime', //更新时间倒序
          isAsc: 'desc',
        },
      };
    },
    onPageScroll(res) {
      //传值到子组件 myPage
      uni.$emit('onPageScroll', res.scrollTop);
    },
    onLoad(options) {
      this.init(options);
    },
    onReachBottom() {
      this.getList(true);
    },
    methods: {
      init(options) {
        this.$log('options', options)
        this.current = parseInt(options.tab) || 0; //记得转换为整数
        this.tabChange(this.current);
      },
      //获取列表
      async getList(nextPage) {
        if (nextPage) {
          if (this.isEnd) return; //到底则不加载
          this.queryParams.pageNum++; //+1页
        } else {
          this.queryParams.pageNum = 1;
        }
        let res = await this.$u.api.listxxx(this.queryParams);
        this.list = nextPage ? [...this.list, ...res.rows] : res.rows;
        this.total = res.total;
        this.isEnd = res.isEnd;
        if (!nextPage) {
          //第一页则滑到最顶部
          this.$scrollTo();
        }
      },
      tabChange(e) {
        this.current = e;
        switch (e) {
            //待发货
          case 0:
            this.queryParams.isPay = '1'; //已支付
            break;
            //已发货
          case 1:
            this.queryParams.isPay = '1'; //已支付
            break;
            //待付款
          case 2:
            this.queryParams.isPay = '0'; //已支付
            break;
            //已完成
        }
        this.getList();
      },
      search(e) {
        this.queryParams.searchValue = e;
        this.getList();
      }
    }
  }
</script>

<style lang="scss" scoped>
</style>


```

<imgPreview src="/develop/uniapp/myPage1.png" :width="629" :height="942" style="margin-top: 20px"/>

### Props属性

| 参数                   | 说明        | 类型            | 默认值                                               |   
|:---------------------|:----------|:--------------|:--------------------------------------------------|
| value                | 搜索框关键词内容  | String        | ''                                                |
| background           | 背景色       | String        | ''                                                |
| marginTop            | 默认距离顶部    | Number\String | ''                                                |
| position             | 位置        | String        | ''                                                |
| zIndex               | 层级        | Number\String | ''                                                |
| top                  | 顶部        | Number\String | ''                                                |
| showSearch           | 是否显示搜索    | Boolean       | false                                             |
| clearabled           | 搜索框是否显示清除 | Boolean       | true                                              |
| disabled             | 搜索框是否禁用   | Boolean       | false                                             |
| searchValue          | 搜索值       | String        | ''                                                |
| showAction           | 是否显示动画    | Boolean       | true                                              |
| searchBgColor        | 搜索框背景色    | String        | 'white'                                           |
| searchPadding        | 搜索框内边距    | Number\String | '12rpx 32rpx'                                     |
| searchFixed          | 搜索框是否固定   | Boolean       | 'false'                                           |
| searchZIndex         | 搜索层级      | Number\String | '100'                                             |
| searchTop            | 搜索距离顶部    | Number\String | ''                                                |
| width                | 宽度        | Number\String | '750'                                             |
| bgColor              | 搜索框背景色    | String        | '#f7f7f7'                                         |
| shape                | 搜索框形状     | String        | 'square'                                          |
| placeholder          | 搜素框填充值    | Number\String | '输入关键词'                                           |
| actionStyle          | 右侧控件样式    | Object        | {}                                                |
| contentMarginTop     | 内容距离顶部    | Number\String | ''                                                |
| nodataMarginTop      | 无内容距离顶部   | Number\String | '160'                                             |
| nodataImg            | 无数据图片地址   | String        | '/static/component/myPage/nodata.png'             |
| total                | 总数        | Number        | ''                                                |
| showTotal            | 是否显示总数    | Boolean       | false                                             |
| tip                  | 空数据       | String        | '暂无数据'                                            |
| isEnd                | 是否结束      | Boolean       | false                                             |
| loadmoreColor        | 加载更多的颜色   | String        | '#999999'                                         |
| loadText             | 加载更多文本    | Object        | {loadmore: '加载更多',loading: '加载中',nomore: '已经到底啦'} |
| loadmoreMarginTop    | 加载更多距离顶部  | String        | '32'                                              |
| loadmoreMarginBottom | 加载更多距离底部  | String        | '32'                                              |
| scrollTop            | 到顶部滚动的距离  | Number        | 0                                                 |

### Event事件

| 事件名     | 说明              | 回调参数 | 
|:--------|:----------------|:-----|
| search  | 搜索点击时触发         | e    |
| change  | 搜索值变化时触发        | e    |
| confirm | 禁用状态 且单击 才触发此方法 | 无    |

