## formMp富文本

::: tip 提示
formMp组件是表单富文本组件
注意: 要使用富文本组件编辑,需要前往pages.json开启路由配置
, //如果要开启富文本编辑,需要打开此页面声明 {
    "path": "components/formMp/mphtml/mphtml",
    "style": {} }
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formMp label="内容" v-model="form.content"></formMp>
      </u-form>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/formMp1.png" :width="617" :height="209" style="margin-top: 20px"/>
<p></p>
<imgPreview src="/develop/uniapp/formMp2.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数        | 说明     | 类型            | 默认值   |   
|:----------|:-------|:--------------|:------|
| value     | 双向绑定的值 | String        | ''    |
| label     | 标签     | String        | ''    |
| required  | 是否必填   | Boolean       | false |
| prop      | 校验属性   | String        | ''    |
| width     | 表单宽度   | Number\String | ''    |
| marginTop | 默认距离顶部 | Number\String | '0'   |

