## myCheck审核

::: tip 提示
myCheck组件是审核组件
:::

### 基本使用

```vue

<template>
  <view>
    <myCheck :show="true"></myCheck>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myCheck1.png" :width="633" :height="134" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myCheck2.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数            | 说明            | 类型      | 默认值   |   
|:--------------|:--------------|:--------|:------|
| show          | 是否显示          | Boolean | false |
| showJjPop     | 是否显示拒绝弹窗      | Boolean | true  |
| showJjConfirm | 是否显示拒绝确认      | Boolean | false |
| showJj        | 是否显示拒绝        | Boolean | true  |
| showBh        | 是否显示驳回        | Boolean | true  |
| marginTop     | 距离顶部          | String  | '16'  |
| tgTip         | 确认的提示文本       | String  | ''    |
| bhTip         | 驳回的提示文本 选择驳回项 | String  | ''    |
| jjTip         | 拒绝的提示文本       | String  | ''    |
| list          | 驳回项           | Array   | []    |

### Event事件

| 事件名     | 说明    | 回调参数                    | 
|:--------|:------|:------------------------|
| confirm | 点击时触发 | form, 包含原因,驳回项,isCheck等 |
