## myModal模态框

::: tip 提示
myModal组件是模态框组件
:::

### 基本使用

```vue

<template>
  <view>
    <myModal title="这是提示" content="这是内容" v-model="showPop">
    </myModal>
    <myBtn title="打开模态框" @confirm="showPop=true"></myBtn>


    <myModal title="这是提示" content="这是内容" v-model="showPop" confirmText="这是确认" cancelText="这是取消">
      <myView>
        这是自定义内容
      </myView>
    </myModal>
    <myBtn title="打开模态框" @confirm="showPop=true"></myBtn>

  </view>
</template>

```

<imgPreview src="/develop/uniapp/myModal1.png" :width="640" :height="1136" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myModal2.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数                | 说明                                   | 类型            | 默认值       |   
|:------------------|:-------------------------------------|:--------------|:----------|
| value             | 弹窗打开双向绑定                             | Boolean       | false     |
| showConfirmButton | 是否显示确认按钮                             | Boolean       | true      |
| confirmText       | 确认文本                                 | String        | '确认'      |
| confirmColor      | 确认文本颜色                               | String        | '#2979ff' |
| confirmStyle      | 确认文本样式                               | Object        | {}        |
| showCancelButton  | 是否显示取消按钮                             | Boolean       | true      |
| cancelText        | 取消文本                                 | String        | '取消'      |
| cancelColor       | 取消文本颜色                               | String        | '#606266' |
| cancelStyle       | 取消文本样式                               | Object        | {}        |
| showTitle         | 是否显示标题                               | Boolean       | true      |
| title             | 标题                                   | String        | ''        |
| titleStyle        | 标题样式                                 | Object        | {}        |
| content           | 内容                                   | String        | ''        |
| customStyle       | 自定义样式                                | Object        | {}        |
| width             | 宽度                                   | Number/String | '600'     |
| zIndex            | 层级                                   | Number/String | 9999      |
| borderRadius      | 圆角                                   | Number/String | '16'      |
| negativeTop       | 往上偏移的值，以避免可能弹出的键盘重合，单位任意，数值则默认为rpx单位 | Number/String | '0'       |
| zoom              | 是否开启缩放模式                             | Boolean       | true      |
| asyncClose        | 是否异步关闭，只对确定按钮有效                      | Boolean       | false     |
| maskCloseAble     | 是否允许点击遮罩关闭Modal                      | Boolean       | true      |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 确认时触发 | 无    |
| cancel  | 取消时触发 | 无    |
