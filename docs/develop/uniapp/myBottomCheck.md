## myBottomCheck底部审核

::: tip 提示
myBottomCheck组件是底部审核组件，主要用来做底部审核，拒绝,驳回,通过等审核
:::

### 基本使用

```vue

<template>
  <view>
    <myBottomCheck :show="true" @confirm="confirm"></myBottomCheck>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myBottomCheck1.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数    | 说明      | 类型      | 默认值   |   
|:------|:--------|:--------|:------|
| show  | 是否显示    | Boolean | false |
| tgTip | 确认的提示文本 | String  | ''    |
| bhTip | 驳回的提示文本 | String  | ''    |
| jjTip | 拒绝的提示文本 | String  | ''    |
| list  | 驳回项     | Array   | []    |

### Event事件

| 事件名     | 说明      | 回调参数               | 
|:--------|:--------|:-------------------|
| confirm | 点击按钮时触发 | form 包含审核状态和原因 驳回项 |
