## myTextArea长文本

::: tip 提示
myTextArea是表单组件，用于输入长文本。
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myTextArea v-model="content"></myTextArea>
      <myTextArea v-model="content" background="red" marginTop="32" placeholder="请输入长文本" :maxlength="200">
      </myTextArea>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myTextArea1.png" :width="628" :height="423" style="margin-top: 20px"/>

### Props属性

| 参数           | 说明     | 类型            | 默认值       |   
|:-------------|:-------|:--------------|:----------|
| label        | 标签     | String        | ''        |
| placeholder  | 占位提示内容 | String        | '请输入内容'   |
| value        | 绑定值    | Number\String | ''        |
| maxlength    | 限制长度   | Number        | 140       |
| background   | 背景色    | String        | '#f7f7f7' |
| color        | 颜色     | String        | '#999999' |
| size         | 字体大小   | Number\String | 28        |
| minHeight    | 最小高度   | Number\String | 148       |
| borderRadius | 圆角     | Number\String | 16        |
| padding      | 内边距    | Number\String | 16        |
| width        | 宽度     | Number\String | 584       |
| marginTop    | 默认距离顶部 | Number\String | '32'      |
| marginRight  | 右边距    | Number\String | ''        |
| marginBottom | 底边距    | Number\String | ''        |
| marginLeft   | 默认距离左侧 | Number\String | '32'      |

