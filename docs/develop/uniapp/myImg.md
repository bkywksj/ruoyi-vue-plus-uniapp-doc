## myImg图片

::: tip 提示
myImg组件是uniapp中比较基础的组件.方便图片,图标显示,点击图片触发,是否确认触发, 是否开启预览,位置方便调整
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myImg src="chat"></myImg>
      <myImg src="chat" size="100" color="red"></myImg>
      <myImg src="/static/me/guanyu.png"></myImg>
      <myImg src="/static/me/guanyu.png" marginTop="32" marginLeft="100"></myImg>
      <myImg src="/static/me/guanyu.png" title="跳转" showConfirm @confirm="confirm"></myImg>
    </myCard>
  </view>
</template>
<script>
  export default {
    methods: {
      confirm() {
        this.$log('点击了图片')
      },
    }
  }
</script>
```

<imgPreview src="/develop/uniapp/myImg1.png" :width="630" :height="352" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myImg2.png" :width="619" :height="368" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明                                                                                                                                                                                                                                                                                                | 类型            | 默认值          |   
|:---------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:-------------|
| src            | 图片地址                                                                                                                                                                                                                                                                                              | String        | ''           |
| size           | 图片尺寸                                                                                                                                                                                                                                                                                              | String        | '40'         |
| mode           | 图片模式.参见 https://uniapp.dcloud.net.cn/component/image.html                                                                                                                                                                                                                                         | String        | 'aspectFill' |
| width          | 提供宽度 size则失效                                                                                                                                                                                                                                                                                      | Number\String | ''           |
| height         | 提供高度size也失效                                                                                                                                                                                                                                                                                       | Number\String | ''           |
| color          | 图片为图标时候的颜色                                                                                                                                                                                                                                                                                        | String        | '#47484d'    |
| title          | 确认标题提示                                                                                                                                                                                                                                                                                            | String        | ''           |
| position       | 相对位置                                                                                                                                                                                                                                                                                              | String        | ''           |
| zIndex         | 层级                                                                                                                                                                                                                                                                                                | Number\String | ''           |
| top            | 顶部                                                                                                                                                                                                                                                                                                | Number\String | ''           |
| right          | 右侧                                                                                                                                                                                                                                                                                                | Number\String | ''           |
| bottom         | 下边                                                                                                                                                                                                                                                                                                | Number\String | ''           |
| left           | 左边                                                                                                                                                                                                                                                                                                | Number\String | ''           |
| borderRadius   | 默认圆角                                                                                                                                                                                                                                                                                              | Number\String | ''           |
| padding        | 默认内边距                                                                                                                                                                                                                                                                                             | Number\String | ''           |
| marginTop      | 默认距离顶部                                                                                                                                                                                                                                                                                            | Number\String | ''           |
| marginRight    | 右边距                                                                                                                                                                                                                                                                                               | Number\String | ''           |
| marginBottom   | 底边距                                                                                                                                                                                                                                                                                               | Number\String | ''           |
| marginLeft     | 默认距离左侧                                                                                                                                                                                                                                                                                            | Number\String | ''           |
| background     | 背景色                                                                                                                                                                                                                                                                                               | Number\String | ''           |
| justifyContent | 对齐                                                                                                                                                                                                                                                                                                | Number\String | 'center'     |
| alignItems     | 对齐                                                                                                                                                                                                                                                                                                | String        | 'center'     |
| filter         | 滤镜 none 高斯模糊blur(rpx)  线性乘法brightness(%) 调整图像的对比度contrast(%)  阴影效果drop-shadow(h-shadow v-shadow blur spread color)  将图像转换为灰度图像grayscale(%) 给图像应用色相旋转hue-rotate(deg) 反转输入图像invert(%) 转化图像的透明程度opacity(%) 转换图像饱和度saturate(%) 将图像转换为深褐色sepia(%) URL函数接受一个XML文件url()如：filter: url(svg-url#element-id) | String        | ''           |
| opacity        | 透明度                                                                                                                                                                                                                                                                                               | String        | ''           |
| transition     | 过渡                                                                                                                                                                                                                                                                                                | String        | ''           |
| bgBorderRadius | 背景圆角 提供rpx单位 因为也有可能是百分比                                                                                                                                                                                                                                                                           | String        | ''           |
| bgPadding      | 背景内边距                                                                                                                                                                                                                                                                                             | String        | ''           |
| bgWidth        | 背景宽度                                                                                                                                                                                                                                                                                              | String        | ''           |
| bgHeight       | 背景高度                                                                                                                                                                                                                                                                                              | String        | ''           |
| preView        | 是否开启预览                                                                                                                                                                                                                                                                                            | Boolean       | false        |
| showConfirm    | 是否需要确认                                                                                                                                                                                                                                                                                            | Boolean       | false        |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
