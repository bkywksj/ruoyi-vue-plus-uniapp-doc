## formAdd表单添加

::: tip 提示
formAdd组件是子项添加组件,添加的项目会自动拼接成逗号隔开的字符串绑定到formAdd组件的v-model上
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <formAdd label="时间1" mode="time" v-model="form.time1"></formAdd>
        <formAdd label="时间2" mode="mutil-column" v-model="form.time2"></formAdd>
        <formAdd label="时间3" mode="item" v-model="form.time3" @add="add"></formAdd>
        <formAdd label="时间4" mode="other" v-model="form.time4" @addItem="addItem"></formAdd>
      </u-form>
    </myContainer>
  </view>
</template>
<script>
  export default {
    methods: {
      add(data, length) {
        console.log("数据", data)
        console.log("长度", length)
      },
      addItem() {
        console.log("自定义处理添加")
      },
    }
  }
</script>
```

<imgPreview src="/develop/uniapp/myFormAdd1.png" :width="616" :height="725" style="margin-top: 20px"/>

### Props属性

| 参数            | 说明                               | 类型            | 默认值   |   
|:--------------|:---------------------------------|:--------------|:------|
| value         | 双向绑定的值                           | String        | ''    |
| width         | 宽度                               | Number\String | ''    |
| marginTop     | 默认距离顶部                           | Number\String | '32'  |
| position      | 位置                               | String        | ''    |
| prop          | 校验属性                             | String        | ''    |
| label         | 标签                               | String        | ''    |
| leftIcon      | 左侧图标                             | String        | ''    |
| leftIconStyle | 左侧图标样式                           | Object        | {}    |
| borderBottom  | 下边线                              | Boolean       | true  |
| required      | 是否必填                             | Boolean       | false |
| inputTip      | 输入提示                             | String        | ''    |
| mode          | 模式 time  mutil-column item other | String        | ''    |
| disabled      | 是否禁用                             | Boolean       | false |
| limit         | 限制个数                             | Number        | 10    |

### Event事件

| 事件名     | 说明                   | 回调参数  | 
|:--------|:---------------------|:------|
| add     | mode为item时候点击时触发     | 数据,长度 |
| addItem | mode为其他模式时候点击加号图标时触发 | 无     |
