## formSelect下拉选择

::: tip 提示
formSelect组件是下拉选择组件
:::

### 基本使用

```vue

<template>
  <view>
    <myContainer>
      <u-form :model="form" ref="form">
        <!--   默认日期选择     -->
        <formSelect label="请选择日期" labelWidth="160" v-model="form.date"></formSelect>
        <!--   列表选择     -->
        <formSelect col label="请选择水果" type="list" :list="[{label:'苹果',value:'1'},{label:'梨',value:'2'}]"
                    showLabel v-model="form.fruit"></formSelect>
        <!--   单选     -->
        <formSelect col label="请选择水果" type="radio" :list="[{label:'苹果',value:'1'},{label:'梨',value:'2'}]"
                    v-model="form.fruit2" dict></formSelect>
        <!--    多选    -->
        <formSelect col label="请选择水果列表" type="checkbox" :list="[{label:'苹果',value:'1'},{label:'梨',value:'2'}]"
                    v-model="form.fruit3"></formSelect>
        <formSelect col label="请选择到分钟" type="minute" v-model="form.minute"></formSelect>
        <!--   多列选择     -->
        <formSelect col label="多列选择" type="mutil-column"
                    :list="[[{label:'江',value:'1'},{label:'湖',value:'2'}],[{label:'夜',value:'3'},{label:'雨',value:'4'}]]"
                    v-model="form.columns" showLabel></formSelect>
        <!--    多列联动    -->
        <formSelect col label="多列联动" type="mutil-column-auto"
                    :list="[{label:'中国',value:'1',children:[{label:'广东',value:'2'},{label:'广西',value:'3'}]}]"
                    v-model="form.columnsAuto" showLabel></formSelect>
        <!--   自定义选择     -->
        <formSelect col label="自定义选择" v-model="form.custom" type="select" @select="customSelect"></formSelect>
      </u-form>
    </myContainer>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/formSelect1.png" :width="626" :height="1223" style="margin-top: 20px"/>

### Props属性

| 参数               | 说明                                                            | 类型            | 默认值             |   
|:-----------------|:--------------------------------------------------------------|:--------------|:----------------|
| value            | 双向绑定的值                                                        | String\Number | ''              |
| col              | 是否竖直方向                                                        | Boolean       | true            |
| width            | 表单宽度                                                          | Number\String | ''              |
| marginTop        | 默认距离顶部                                                        | Number\String | '0'             |
| position         | 相对位置                                                          | String        | ''              |
| prop             | 校验属性                                                          | String        | ''              |
| label            | 标签                                                            | String        | ''              |
| labelWidth       | 标签宽度                                                          | Number\String | ''              |
| labelStyle       | 标签样式                                                          | Object        | {}              |
| labelAlign       | 标签对齐方式                                                        | String        | ''              |
| leftIcon         | 左侧图标                                                          | String        | ''              |
| leftIconStyle    | 左侧图标样式                                                        | Object        | {}              |
| rightIcon        | 右侧图标                                                          | String        | ''              |
| rightIconStyle   | 右侧图标样式                                                        | Object        | {}              |
| borderBottom     | 是否显示下划线                                                       | Boolean       | true            |
| required         | 是否必填                                                          | Boolean       | false           |
| background       | 背景颜色                                                          | String        | ''              |
| borderRadius     | 圆角                                                            | String        | ''              |
| disabled         | 是否禁用                                                          | Boolean       | false           |
| placeholder      | 填充值                                                           | String        | ''              |
| placeholderStyle | 填充样式                                                          | String        | 'color:#CACFDB' |
| customStyle      | 自定义样式                                                         | Object        | {}              |
| type             | 选择器的类型 取值 date list radio checkbox minute分 region地区 select自定义 | String        | 'date'          |
| list             | 当type设置为list或多列的时候，就是弹出选择列表进行选择 必须提供list属性                    | Array         | []              |
| width            | radio单选 checkbox多选时候的单个选项的宽度 默认205rpx                         | String        | '205'           |
| dict             | 是否进行字典解析 为真则将list中的value进行字典解析                                | Boolean       | false           |                                                  | Boolean       | false           |
| showLabel        | 显示标签值                                                         | Boolean       | false           |

### Event事件

| 事件名    | 说明                 | 回调参数 | 
|:-------|:-------------------|:-----|
| select | type为select的时候点击触发 | 无    |
