## myCartStatus购物车状态

::: tip 提示
myCartStatus组件是我的购物车状态 展示购物车金额和去结算按钮 购物车有东西 并且购物车price大于0
需要在vuex中实现$cartPrice()方法
:::

### 基本使用

```vue

<template>
  <view>
    <myCartStatus :cart="$cart()"></myCartStatus>
  </view>
</template>

```

### Props属性

| 参数   | 说明    | 类型    | 默认值 |   
|:-----|:------|:------|:----|
| cart | 购物车内容 | Array | []  |

### Event事件

| 事件名      | 说明    | 回调参数 | 
|:---------|:------|:-----|
| showCart | 显示购物车 | 无    |
