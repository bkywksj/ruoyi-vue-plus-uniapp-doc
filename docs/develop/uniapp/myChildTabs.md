## myChildTabs子标签

::: tip 提示
myChildTabs组件是二级标签组件,可以配合myTabs使用,作为myTabs的子菜单使用
:::

### 基本使用

```vue

<template>
  <view>
    <myTabs :tabs="tabs" :current="current" @change="tabChange"/>
    <myChildTabs :list="[{label:'未处理',status:'0',num:1},{label:'已处理',status:'1',num:2}]" v-if="current===0">
    </myChildTabs>
  </view>
</template>
<script>
  export default {
    data() {
      return {
        current: 0,
        tabs: [{
          name: '手机',
          count: 10
        }, {
          name: '电视',
          count: 2
        }, {
          name: '家电'
        }, {
          name: '笔记本'
        }],
      }
    },
  }
</script>
```

<imgPreview src="/develop/uniapp/myTabsChild1.png" :width="635" :height="211" style="margin-top: 20px"/>

### Props属性

| 参数        | 说明     | 类型            | 默认值    |   
|:----------|:-------|:--------------|:-------|
| position  | 位置     | String        | ''     |
| fixed     | 是否固定   | Boolean       | false  |
| marginTop | 距离顶部   | Number\String | ''     |
| height    | 高度     | Number\String | '96'   |
| top       | 顶部     | Number\String | ''     |
| right     | 右侧     | Number\String | ''     |
| bottom    | 下边     | Number\String | ''     |
| left      | 左侧     | Number\String | ''     |
| zIndex    | 层级     | String        | '9999' |
| width     | 宽度     | Number\String | '750'  |
| current   | 当前激活索引 | Number\String | 0      |
| list      | 绑定数组   | Array         | []     |

### Event事件

| 事件名     | 说明        | 回调参数                   | 
|:--------|:----------|:-----------------------|
| confirm | 切换子tab时触发 | activeItem, currentTab |
