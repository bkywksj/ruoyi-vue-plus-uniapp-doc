## myDiamon金刚区

::: tip 提示
myDiamon组件是金刚区布局组件
:::

### 基本使用

```vue

<template>
  <view>
    <myDiamon
        :list="[{name:'分类1',img:'chat'},{name:'分类2',img:'chat'},{name:'分类3',img:'chat'},{name:'分类4',img:'chat'},{name:'分类5',img:'chat'},{name:'分类6',img:'chat'}]"
        @confirm="confirm">
    </myDiamon>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myDiamon1.png" :width="640" :height="348" style="margin-top: 20px"/>

### Props属性

| 参数   | 说明    | 类型    | 默认值 |   
|:-----|:------|:------|:----|
| list | 金刚区内容 | Array | []  |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | item |
