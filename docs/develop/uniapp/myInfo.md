## myInfo信息

::: tip 提示
myInfo组件是信息展示组件
:::

### 基本使用

```vue

<template>
  <view>
    <myCard>
      <myInfo title="标题1" content="内容1"></myInfo>
      <myInfo col title="标题2" content="内容2"></myInfo>
      <myInfo title="标题3" content="内容3,内容4" split></myInfo>
      <myInfo col title="标题4" content="内容5,内容6" split></myInfo>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myInfo1.png" :width="631" :height="545" style="margin-top: 20px"/>

### Props属性

| 参数           | 说明           | 类型            | 默认值   |   
|:-------------|:-------------|:--------------|:------|
| col          | 是否竖直方向       | Boolean       | false |
| title        | 标题           | String        | ''    |
| content      | 内容           | String        | ''    |
| marginTop    | 默认距离顶部       | Number\String | '32'  |
| background   | 背景色          | Number\String | ''    |
| titleType    | 文字颜色类型 1 2 3 | String        | '1'   |
| titleColor   | 文字颜色         | String        | ''    |
| contentType  | 内容文字颜色类型     | String        | '1'   |
| contentColor | 内容文字颜色       | String        | '1'   |
| near         | 内容是否在标题旁边    | Boolean       | false |
| split        | 是否分割内容       | Boolean       | false |
| showBorder   | 是否显示边框       | Boolean       | false |
| copy         | 是否可以拷贝       | Boolean       | false |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | item |
