## 概述

::: warning 注意
uniapp端相关组件, 主要是为了简化css样式的编写. 迁移代码的时候更加方便,
也不用担心css代码冲突,只有组件自身无法通过属性实现的, 比如动画等,才使用view等系统组件和css样式来实现
绝大多数情况下,可以实现0css代码的编写, 在写代码过程中, 能实现抽离成组件的尽量抽离成组件, 这样方便维护,
每个页面根据需求拆分成若干组件, 注释一定得写清楚.
:::
