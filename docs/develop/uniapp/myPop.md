## myPop弹窗

::: tip 提示
myPop组件是弹窗组件
:::

### 基本使用

```vue

<template>
  <view>
    <myPop v-model="showPop"></myPop>
    <myBtn title="打开弹窗" @confirm="showPop=true"></myBtn>

    <myPop v-model="showPop2" isText></myPop>
    <myBtn title="打开弹窗" @confirm="showPop2=true"></myBtn>

    <myPop v-model="showPop3" :list="[{label:'理由1',value:'1'},{label:'理由2',value:'2'}]"></myPop>
    <myBtn title="打开弹窗" @confirm="showPop3=true"></myBtn>

  </view>
</template>

```

<imgPreview src="/develop/uniapp/myPop1.png" :width="640" :height="1136" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myPop2.png" :width="640" :height="1136" style="margin-top: 20px"/>
<imgPreview src="/develop/uniapp/myPop3.png" :width="640" :height="1136" style="margin-top: 20px"/>

### Props属性

| 参数          | 说明               | 类型      | 默认值                                                                                 |   
|:------------|:-----------------|:--------|:------------------------------------------------------------------------------------|
| title       | 弹窗标题             | String  | ''                                                                                  |
| tip         | 弹窗提示             | String  | ''                                                                                  |
| required    | 是否必填             | Boolean | true                                                                                |
| inputTip    | 输入提示             | String  | ''                                                                                  |
| label       | 标签               | String  | ''                                                                                  |
| value       | 弹窗打开双向绑定         | Boolean | false                                                                               |
| isText      | 是否长文本            | Boolean | false                                                                               |
| isClear     | 是否带清空            | Boolean | true                                                                                |
| maxlength   | 最长长度             | Number  | 140                                                                                 |
| content     | 内容               | String  | ''                                                                                  |
| customStyle | 自定义样式            | Object  | {background: '#f7f7f7',paddingLeft: '16rpx',borderRadius: '8rpx',color: '#47484D',} |
| list        | 驳回项选择可以使用        | Array   | []                                                                                  |
| width       | checkbox宽度 默认50% | String  | '50%'                                                                               |

### Event事件

| 事件名     | 说明    | 回调参数                         | 
|:--------|:------|:-----------------------------|
| confirm | 点击时触发 | data,rejectReason,rejectItem |
