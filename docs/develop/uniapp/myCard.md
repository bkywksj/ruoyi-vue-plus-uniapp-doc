## myCard卡片

::: tip 提示
myCard组件是uniapp中比较基础的组件.可以方便实现卡片效果,自带边距和内边距,圆角等
:::

### 基本使用

```vue

<template>
  <view>
    <myCard display="flex" col>
      <myText text="这是标题" size="40"></myText>
      <myText text="这是内容..." size="24"></myText>
    </myCard>
  </view>
</template>

```

<imgPreview src="/develop/uniapp/myCard1.png" :width="620" :height="166" style="margin-top: 20px"/>

### Props属性

| 参数             | 说明       | 类型            | 默认值     |   
|:---------------|:---------|:--------------|:--------|
| col            | 是否竖直方向   | Boolean       | false   |
| position       | 相对位置     | String        | ''      |
| display        | 盒子模型 默认无 | String        | ''      |
| zIndex         | 层级       | Number\String | ''      |
| top            | 顶部       | Number\String | ''      |
| right          | 右侧       | Number\String | ''      |
| bottom         | 下边       | Number\String | ''      |
| left           | 左边       | Number\String | ''      |
| width          | 默认宽度686  | Number\String | ''      |
| height         | 默认高度     | Number\String | ''      |
| borderRadius   | 默认圆角     | Number\String | '16'    |
| padding        | 默认内边距    | Number\String | '32'    |
| marginTop      | 默认距离顶部   | Number\String | '32'    |
| marginRight    | 右边距      | Number\String | ''      |
| marginBottom   | 底边距      | Number\String | ''      |
| marginLeft     | 默认距离左侧   | Number\String | '32'    |
| background     | 背景色      | Number\String | 'white' |
| justifyContent | 对齐       | Number\String | ''      |
| alignItems     | 对齐       | String        | ''      |
| transition     | 过渡       | String        | ''      |
| boxShadow      | 阴影       | String        | ''      |

### Event事件

| 事件名     | 说明    | 回调参数 | 
|:--------|:------|:-----|
| confirm | 点击时触发 | 无    |
