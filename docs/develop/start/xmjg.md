## 起步

### 后端项目结构

RuoYi-Vue-Plus-uniapp  
├─ruoyi-admin // 管理模块  
├─ruoyi-common // 通用模块  
│ └─ ruoyi-common-bom // common依赖包管理  
│ └─ ruoyi-common-core // 核心模块  
│ └─ ruoyi-common-doc // 系统接口模块  
│ └─ ruoyi-common-encrypt // 数据加解密模块  
│ └─ ruoyi-common-excel // excel模块  
│ └─ ruoyi-common-http // forest网络工具模块  
│ └─ ruoyi-common-idempotent // 幂等功能模块  
│ └─ ruoyi-common-job // 定时任务模块  
│ └─ ruoyi-common-json // 序列化模块  
│ └─ ruoyi-common-log // 日志模块  
│ └─ ruoyi-common-mail // 邮件模块  
│ └─ ruoyi-common-miniapp // 微信小程序模块  
│ └─ ruoyi-common-mp // 微信公众号模块  
│ └─ ruoyi-common-mybatis // 数据库模块  
│ └─ ruoyi-common-oss // oss服务模块  
│ └─ ruoyi-common-poster // 海报服务模块  
│ └─ ruoyi-common-pay // 支付服务模块  
│ └─ ruoyi-common-ratelimiter // 限流功能模块  
│ └─ ruoyi-common-redis // 缓存服务模块  
│ └─ ruoyi-common-satoken // satoken模块  
│ └─ ruoyi-common-security // 安全模块  
│ └─ ruoyi-common-sensitive // 脱敏模块  
│ └─ ruoyi-common-sms // 短信模块  
│ └─ ruoyi-common-social // 社交三方模块  
│ └─ ruoyi-common-translation // 通用翻译模块  
│ └─ ruoyi-common-web // web模块  
│ └─ ruoyi-common-websocket // websocket服务集成模块  
├─ruoyi-extend // 监控模块  
│ └─ ruoyi-monitor-admin // admin监控模块  
│ └─ ruoyi-xxl-job-admin // 任务调度中心模块  
├─ruoyi-modules // 扩展模块  
│ └─ ruoyi-core // 核心模块  
│ └─ ruoyi-generator // 代码生成模块  
│ └─ ruoyi-service // 业务模块  
│ └─ ruoyi-system // 系统模块  
├─ruoyi-ui // 前端框架  
└─ruoyi-uniapp // 小程序框架

### ruoyi-ui端项目结构

<imgPreview src="/develop/start/ui_xmjg.png" style="margin-top: 20px"/>

### uniapp端项目结构

<imgPreview src="/develop/start/uniapp_xmjg.png" style="margin-top: 20px"/>
