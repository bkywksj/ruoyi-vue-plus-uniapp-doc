## 项目运行

### 1.项目唯一标识符修改

::: tip 重要提示
项目唯一标识符是uniapp-2024, 下载源码后, 务必全局搜索并修改为自己的项目唯一标识符比如mall,
项目唯一标识符默认了很多东西, 比如数据库名称, 应用名, 打包后的jar包名称, redis缓存的前缀, 文件上传的前缀, 反向代理的路径,
docker部署的容器名称等, 一个项目使用一个唯一标识符
:::

<imgPreview src="/develop/run/run1.png" style="margin-top: 20px"/>

### 2.全局搜索5400, 注意必须打开单词搜索 , 修改为自己的端口号, 一般一个项目占用一个唯一端口, 这样可以防止多项目运行冲突

::: tip 提示
必须打开W单词搜索, 防止替换错了地方, 因为有的地方也包含5400
:::

<imgPreview src="/develop/run/run2.png" style="margin-top: 20px"/>

### 3.新建数据库, 名称为唯一标识符比如mall, 导入数据库脚本, 

::: tip 提示
数据库脚本在script目录下uniapp-2024.sql 也改名为mall.sql
:::

### 4.选择jdk1.8, 点击运行或调试(一般点调试, 方便后续打断点)

::: tip 提示
jdk1.8最好是202版本-免费的最后一个版本,过低版本会导致hutool加密模块报错
:::

### 5. 使用idea打开ruoyi-ui目录, 点击readme, 安装依赖, 点击根目录下的package.json, 然后点击scripts里面的dev左侧的小三角即可快速启动

::: tip 提示
readme里面,npm install --registry=https://registry.npmmirror.com 左侧有小三角, 
package.json里面的scripts里面的dev左侧也有小三角, 点击可以快速触发操作( 此时后端和前端均启动成功 )
:::

### 6. 使用hbuilderx打开ruoyi-uniapp目录, 配置微信开发者工具, 点击运行, 运行到小程序模拟器, 运行到微信开发者工具

::: tip 提示
因为每个项目的uniapp目录都是ruoyi-uniapp, 所以最好给他们起个别名, 比如mall
:::

<imgPreview src="/develop/run/run3.png" style="margin-top: 20px"/>

<imgPreview src="/develop/run/run4.png" style="margin-top: 20px"/>
