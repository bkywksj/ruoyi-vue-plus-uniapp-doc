## 修改jar包数据库密码

### 双击school.jar使用压缩文件直接打开

<imgPreview src="/develop/db/db11.jpg" style="margin-top: 20px"/>

### 找到如图目录下的application-prod配置文件

<imgPreview src="/develop/db/db12.jpg" style="margin-top: 20px"/>

### 修改密码

<imgPreview src="/develop/db/db13.jpg" style="margin-top: 20px"/>

### 点击更新压缩包

<imgPreview src="/develop/db/db14.jpg" style="margin-top: 20px"/>
