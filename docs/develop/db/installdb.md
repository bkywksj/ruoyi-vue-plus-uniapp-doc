## 数据库安装

### 点击数据库，添加数据库，填写信息，如图

<imgPreview src="/develop/db/db1.jpg" style="margin-top: 20px"/>

### 点击导入数据库，如图

<imgPreview src="/develop/db/db2.jpg" style="margin-top: 20px"/>

### 上传数据库.sql文件，导入

<imgPreview src="/develop/db/db3.jpg" style="margin-top: 20px"/>
