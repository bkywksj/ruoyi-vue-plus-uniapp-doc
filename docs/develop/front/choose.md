## 下拉选择表单组件

::: tip 提示
下拉选择表单组件
:::

### 基本使用

```vue

<template>
  <div class="app-container">
    <el-form :model="queryParams" ref="queryForm" :inline="true" v-show="showSearch" label-width="70px">
      <choose label="appid" prop="appid" v-model="queryParams.appid" :options="appidOptions"
              @change="handleQuery"/>
    </el-form>
    <!--  弹窗   -->
    <el-dialog :title="title" :visible.sync="open" width="1000px" append-to-body class="scrollbar">
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
        <el-row>
          <choose label="appid" prop="appid" :span="12" v-model="form.appid" :options="appidOptions"/>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button :loading="buttonLoading" type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
  </div>
</template>

```

### Props属性

| 参数          | 说明                     | 类型                    | 默认值   |   
|:------------|:-----------------------|:----------------------|:------|
| value       | 绑定的值                   | Number\String\Boolean | 无     |
| label       | 标签                     | String                | 日期    |
| placeholder | 表单占位符                  | String                | 无     |                                             
| prop        | prop属性, 用于表单校验         | String                | 无     |                                             
| showForm    | 为true则外包一层el-form-item | Boolean               | true  |                                             
| allowCreate | 是否允许创建                 | Boolean               | false |                                             
| filterable  | 是否开启过滤                 | Boolean               | true  |                                             
| clearable   | 是否可清除                  | Boolean               | true  | 
| disabled    | 是否禁用                   | Boolean               | false | 
| multiple    | 是否可多选                  | Boolean               | false | 
| size        | 组件大小,默认small           | String                | small |
| options     | 多选框可选值                 | Array                 | 无     |
| span        | span宽度                 | Number                | 无     |
| showValue   | 下拉框是否显示value值          | Boolean               | true  |

### Event事件

| 事件名    | 说明          | 回调参数        | 
|:-------|:------------|:------------|
| input  | 选择时触发, 双向绑定 | e, 选择值组成字符串 |
| change | 改变时触发       | e, 选择值组成字符串 |
| focus  | 聚焦时触发       | 无           |
