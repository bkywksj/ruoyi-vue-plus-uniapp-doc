## 输入框表单组件

::: tip 提示
输入框表单组件
:::

### 基本使用

```vue

<template>
  <div class="app-container">
    <el-form :model="queryParams" ref="queryForm" :inline="true" v-show="showSearch" label-width="70px">
      <in label="模糊搜索" prop="searchValue" v-model="queryParams.searchValue" @input="handleQuery"/>
    </el-form>
    <!--  弹窗   -->
    <el-dialog :title="title" :visible.sync="open" width="1000px" append-to-body class="scrollbar">
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
        <el-row>
          <in label="名称" prop="goodsName" :span="12" v-model="form.goodsName"/>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button :loading="buttonLoading" type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
  </div>
</template>

```

### Props属性

| 参数               | 说明                          | 类型            | 默认值                      |   
|:-----------------|:----------------------------|:--------------|:-------------------------|
| value            | 绑定的值                        | Number\String | 无                        |
| label            | 标签                          | Number\String | 宽度                       |
| width            | 表单占位符                       | String        | 无                        |                                             
| prop             | prop属性, 用于表单校验              | String        | 无                        |                                             
| maxlength        | 最大长度                        | Number\String | -                        |                                             
| disabled         | 是否禁用                        | Boolean       | false                    | 
| showForm         | 为true则外包一层el-form-item      | Boolean       | true                     |                                             
| showPassword     | 是否显示密码                      | Boolean       | false                    |                                             
| type             | 类型                          | String        | text                     |                                             
| autosize         | 自动尺寸用于type为textarea时        | Object        | {minRows: 2,maxRows: 30} |      
| placeholder      | 表单占位符                       | String        | 无                        |   
| clearable        | 是否可清除                       | Boolean       | true                     | 
| size             | 组件大小,默认small                | String        | small                    |
| span             | span宽度                      | Number        | 无                        |
| min              | type=number时计数器最小值          | Number        | 无                        |
| max              | type=number时计数器最大值          | Number        | 无                        |
| step             | type=number时计数器步长           | Number        | 1                        |
| stepStrictly     | type=number时是否只能输入 step 的倍数 | Boolean       | false                    |                                             
| precision        | type=number时数值精度            | Number        | 无                        |                                             
| controls         | type=number时是否使用控制按钮        | Boolean       | true                     |                                             
| controlsPosition | type=number时控制按钮位置          | String        | right                    |                                             
| inputNumberSize  | type=number时计数器尺寸           | String        | medium                   |                                             

### Event事件

| 事件名    | 说明            | 回调参数  | 
|:-------|:--------------|:------|
| input  | 选择时触发, 双向绑定   | e, 内容 |
| blur   | 失去焦点时触发, 双向绑定 | e, 内容 |
| change | 改变时触发         | e, 内容 |
| enter  | 回车时触发         | e, 内容 |
