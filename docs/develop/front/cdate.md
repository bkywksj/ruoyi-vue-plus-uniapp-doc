## 日期选择表单组件

::: tip 提示
日期表单组件, 可用于搜索栏时间区间, 也可用于弹窗中的表单填写.
:::

### 基本使用

```vue

<template>
  <div class="app-container">
    <!--  搜索栏里面的时间选择  -->
    <el-form :model="queryParams" ref="queryForm" :inline="true" v-show="showSearch" label-width="70px">
      <cdate label="创建时间" prop="createTime" v-model="dateRangeCreateTime" type="daterange" format="yyyy-MM-dd"
             @change="handleQuery"/>
    </el-form>

    <!--  弹窗里面的时间表单   -->
    <el-dialog :title="title" :visible.sync="open" width="1000px" append-to-body class="scrollbar">
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
        <el-row>
          <cdate label="时间" prop="time" :span="12" v-model="form.time"/>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button :loading="buttonLoading" type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
  </div>
</template>

//script脚本中
// 创建时间时间范围
dateRangeCreateTime: [],

//构造查询参数
this.addDateRange(this.queryParams, this.dateRangeCreateTime, 'CreateTime')
```

<imgPreview src="/develop/front/cdate.png" :width="545" :height="289" style="margin-top: 20px"/>
<imgPreview src="/develop/front/cdate2.png" :width="385" :height="370" style="margin-top: 20px"/>

### Props属性

| 参数          | 说明                                                                                    | 类型                  | 默认值      |   
|:------------|:--------------------------------------------------------------------------------------|:--------------------|:---------|
| value       | 绑定的时间                                                                                 | Number\String\Array | 无        |
| label       | 标签                                                                                    | String              | 日期       |
| width1      | 有span时生效, 表单宽度, 弹窗表单时间选择默认如此                                                          | Number \ String     | 220      |                                                          
| width2      | 无span时生效, 表单宽度, 搜索栏时间选择默认如此                                                           | Number \ String     | 100%     |                                                       
| prop        | prop属性, 用于表单校验                                                                        | String              | 无        |                                             
| clearable   | 是否可清除日期                                                                               | Boolean             | false    | 
| disabled    | 是否禁用日期                                                                                | Boolean             | false    |
| size        | 组件大小,默认small                                                                          | String              | small    |
| type        | 类型year/month/date/dates/months/years week/datetime/datetimerange/daterange/monthrange | String              | datetime |
| format      | 时间格式化(TimePicker)                                                                     | String              | 无        |
| valueFormat | 仅TimePicker时可用，绑定值的格式。不指定则绑定值为 Date 对象                                                | String              | 无        |
| span        | span宽度                                                                                | Number              | 无        |

### Event事件

| 事件名    | 说明            | 回调参数     | 
|:-------|:--------------|:---------|
| input  | 选择时间时触发, 双向绑定 | e, 选择的时间 |
| change | 选择时间时候触发      | e, 选择的时间 |
