## 文件上传表单组件

::: tip 提示
文件上传表单组件
:::

### 基本使用

```vue

<template>
  <div class="app-container">
    <!--  弹窗里面的时间表单   -->
    <el-dialog :title="title" :visible.sync="open" width="1000px" append-to-body class="scrollbar">
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
        <el-row>
          <fUpload label="文件上传" prop="fileUrl" :span="12" v-model="form.fileUrl"/>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button :loading="buttonLoading" type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
  </div>
</template>

```

### Props属性

| 参数          | 说明             | 类型            | 默认值 |   
|:------------|:---------------|:--------------|:----|
| value       | 绑定的值           | Number\String | 无   |
| label       | 标签             | String        | 选项  |
| placeholder | 表单占位符          | String        | 无   |                                             
| prop        | prop属性, 用于表单校验 | String        | 无   |                                             
| limit       | 文件上传限制数量       | Number        | 1   |                                             
| span        | span宽度         | Number        | 无   |

### Event事件

| 事件名   | 说明          | 回调参数  | 
|:------|:------------|:------|
| input | 选择时触发, 双向绑定 | e, 内容 |
