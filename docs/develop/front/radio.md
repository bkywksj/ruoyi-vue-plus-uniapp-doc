## 单选框表单组件

::: tip 提示
单选框表单组件是基于el-radio组件的二次封装
:::

### 基本使用

```vue

<template>
  <div class="app-container">
    <!--  弹窗   -->
    <el-dialog :title="title" :visible.sync="open" width="1000px" append-to-body class="scrollbar">
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
        <el-row>
          <radio label="状态" prop="status" :span="12" v-model="form.status" :options="dict.type.sys_normal_disabled"/>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button :loading="buttonLoading" type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
  </div>
</template>

```

### Props属性

| 参数       | 说明             | 类型                    | 默认值   |   
|:---------|:---------------|:----------------------|:------|
| value    | 绑定的值           | Number\String\Boolean | 无     |
| label    | 标签             | String                | 单选    |
| prop     | prop属性, 用于表单校验 | String                | 无     |                                             
| disabled | 是否禁用           | Boolean               | false | 
| options  | 单选框可选值         | Array                 | []    |

### Event事件

| 事件名    | 说明          | 回调参数        | 
|:-------|:------------|:------------|
| input  | 选择时触发, 双向绑定 | e, 选择值组成字符串 |
| change | 改变时触发       | e, 选择值组成字符串 |
