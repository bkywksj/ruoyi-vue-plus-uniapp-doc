## 富文本表单组件

::: tip 提示
富文本表单组件
:::

### 基本使用

```vue

<template>
  <div class="app-container">
    <!--  弹窗里面的时间表单   -->
    <el-dialog :title="title" :visible.sync="open" width="1000px" append-to-body class="scrollbar">
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
        <el-row>
          <rich label="内容" v-model="form.content"/>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button :loading="buttonLoading" type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
  </div>
</template>

```

### Props属性

| 参数        | 说明     | 类型     | 默认值 |   
|:----------|:-------|:-------|:----|
| value     | 绑定的值   | String | ''  |
| label     | 标签     | String | 富文本 |
| span      | span宽度 | Number | 无   |
| minHeight | 最小高度   | Number | 192 |                                             

### Event事件

| 事件名   | 说明          | 回调参数  | 
|:------|:------------|:------|
| input | 输入时触发, 双向绑定 | e, 内容 |
